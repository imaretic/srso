Backend part of the final work project.
To run this NodeJS project, run: `npm instal` and `npm start`.
Using Express.js and Sequelize.

---

# System for reservation of sport objects

## Abstract

The topic of this thesis is the creation of system for reservation of sport objects.
Introductory chapter contains description of the problem and the proposed solution. In
the next chapter, all used technologies in this project are described: HTML, SCSS,
NodeJS, React, PostgreSQL and REST API principles. Data model, server and client
parts of the project are described in the architecture chapter. The server part of the
project was written in Javascript (NodeJS) and includes Sequelize package for
database management and Express.js as server core. Client part of the project was
written in HTML, SCSS and Javascript library React. The last chapter describes how
the system works from user's perspective.

**Keywords**: web application, nodejs, react

# Sustav za rezervaciju termina sportskih objekata

## Sažetak

Tema ovog završnog rada je izrada sustava za rezervaciju termina sportskih objekata.
Uvodni dio sadrži opis problema te način na koji se problem može riješiti. U sljedećem
poglavlju opisane su tehnologije koje su korištene pri izradi: HTML, SCSS, NodeJS,
React, PostgreSQL te REST API principi. U poglavlju o arhitekturi opisani su modeli
podataka te poslužiteljski i klijentski dijelovi sustava. Poslužiteljski dio aplikacije izvodi
se pomoću NodeJS-a, a koristi pakete Sequelize za upravljanje bazom podataka te
Express.js za izradu web-poslužitelja. Klijentski dio aplikacije napisan je koristeći
HTML, SCSS te Javascript biblioteku React. Poglavlje o radu sustava opisuje na koji
način sustav radi iz korisničke perspektive.

**Ključne riječi**: web-aplikacija, nodejs, react
