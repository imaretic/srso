const User = require("../models/user");
const Company = require("../models/company");
const { validationResult } = require("express-validator/check");

exports.getCompanies = (req, res, next) => {
  Company.findAll()
    .then(company => {
      res.status(200).json(company);
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.createCompany = (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    const error = new Error("Validation failed.");
    error.statusCode = 422;
    error.data = errors.array();
    throw error;
  }

  const name = req.body.name;
  const email = req.body.email;
  const phone = req.body.phone;
  const street = req.body.street;
  const streetNumber = req.body.streetNumber;
  const city = req.body.city;
  const postCode = req.body.postCode;
  const country = req.body.country;

  let savedUser;
  User.findByPk(req.userId)
    .then(user => {
      if (!user) {
        const error = new Error("User does not exist.");
        error.statusCode = 500;
        throw error;
      }
      return user;
    })
    .then(user => {
      savedUser = user;
      return user.getCompany();
    })
    .then(company => {
      if (company) {
        const error = new Error("User already has a company.");
        error.statusCode = 403;
        throw error;
      }
      return savedUser;
    })
    .then(user => {
      return user.createCompany({
        name,
        email,
        phone,
        street,
        streetNumber,
        city,
        postCode,
        country
      });
    })
    .then(company => {
      res.status(200).json({ message: "Company crated", company });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.editCompany = (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    const error = new Error("Validation failed.");
    error.statusCode = 422;
    error.data = errors.array();
    throw error;
  }

  const name = req.body.name;
  const email = req.body.email;
  const phone = req.body.phone;
  const street = req.body.street;
  const streetNumber = req.body.streetNumber;
  const city = req.body.city;
  const postCode = req.body.postCode;
  const country = req.body.country;

  const companyId = req.params.id;

  Company.findByPk(companyId)
    .then(company => {
      return company.update({
        name,
        email,
        phone,
        street,
        streetNumber,
        city,
        postCode,
        country
      });
    })
    .then(company => {
      res.json(company);
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.deleteCompany = (req, res, next) => {
  const companyId = req.params.id;

  Company.findByPk(companyId)
    .then(company => {
      if (company === null) {
        const error = new Error("Company does not exist.");
        error.statusCode = 404;
        throw error;
      } else {
        return company.destroy();
      }
    })
    .then(() => {
      res.status(200).json({ message: "Company deleted!" });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.getCompanyByUser = (req, res, next) => {
  const userId = req.params.id;
  User.findByPk(userId)
    .then(user => {
      if (user === null) {
        const error = new Error("User does not exist.");
        error.statusCode = 404;
        throw error;
      } else {
        return user.getCompany();
      }
    })
    .then(company => {
      if (company === null) {
        const error = new Error("User does not have a company.");
        error.statusCode = 404;
        throw error;
      } else {
        res.status(200).json(company);
      }
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.getCompany = (req, res, next) => {
  const companyId = req.params.id;
  Company.findByPk(companyId)
    .then(company => {
      if (company === null) {
        const error = new Error("Company does not exist.");
        error.statusCode = 404;
        throw error;
      }
      res.json(company);
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};
