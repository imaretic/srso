const SportField = require("../models/sportfield");
const Company = require("../models/company");
const Sport = require("../models/sport");
const { validationResult } = require("express-validator/check");

exports.getFields = (req, res, next) => {
  SportField.findAll()
    .then(sportFields => {
      res.status(200).json(sportFields);
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.getFieldsByCompany = (req, res, next) => {
  const companyId = req.params.id;
  Company.findByPk(companyId)
    .then(company => {
      if (company === null) {
        const error = new Error("Company does not exist.");
        error.statusCode = 404;
        throw error;
      }
      return SportField.findAll({
        where: { companyId: companyId },
        include: [
          {
            model: Sport
          }
        ]
      });
    })
    .then(fields => {
      if (fields === null) {
        const error = new Error("Company does not have any fields.");
        error.statusCode = 404;
        throw error;
      } else {
        res.status(200).json(fields);
      }
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.getField = (req, res, next) => {
  const fieldId = req.params.id;
  Field.findByPk(fieldId)
    .then(field => {
      res.status(200).json(field);
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.putCreate = (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    const error = new Error("Validation failed.");
    error.statusCode = 422;
    error.data = errors.array();
    throw error;
  }

  const name = req.body.name;
  const numberOfPlayers = req.body.numberOfPlayers;
  const conditions = req.body.conditions;
  const sportId = req.body.sportId;
  const companyId = req.body.companyId;

  Company.findByPk(companyId)
    .then(company => {
      if (!company) {
        const error = new Error("Company does not exist.");
        error.statusCode = 500;
        throw error;
      }
      return company;
    })
    .then(company => {
      return SportField.create({
        name,
        numberOfPlayers,
        conditions,
        companyId,
        sportId
      });
    })
    .then(sportfield => {
      if (!sportfield) {
        const error = new Error("Field could not be created.");
        error.statusCode = 500;
        throw error;
      }
      return SportField.findByPk(sportfield.id, {
        include: [
          {
            model: Sport
          }
        ]
      });
    })
    .then(sportfield => {
      res.status(200).json({ message: "Sport field created", sportfield });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.getField = (req, res, next) => {
  const fieldId = req.params.id;
  let savedField;
  SportField.findByPk(fieldId)
    .then(field => {
      if (field === null) {
        const error = new Error("Field does not exist.");
        error.statusCode = 404;
        throw error;
      } else {
        savedField = field;
        return Company.findOne({
          where: {
            id: field.companyId
          }
        });
      }
    })
    .then(company => {
      res.status(200).json({ field: savedField, company });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

/**
 * Delete certain field.
 */
exports.deleteField = (req, res, next) => {
  const fieldId = req.params.id;

  SportField.findByPk(fieldId)
    .then(field => {
      return field.destroy();
    })
    .then(() => {
      res.status(200).json({ message: "Field deleted successfully!" });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};
