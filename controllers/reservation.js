const Reservation = require("../models/reservation");
const Timeslot = require("../models/timeslot");
const Sportfield = require("../models/sportfield");
const Company = require("../models/company");
const Participation = require("../models/participation");
const User = require("../models/user");
const { validationResult } = require("express-validator/check");

exports.getReservations = (req, res, next) => {
  Reservation.findAll()
    .then(reservations => {
      res.status(200).json(reservations);
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.getReservationsByUser = (req, res, next) => {
  const userId = req.params.id;

  Participation.findAll({
    where: {
      userId: userId
    },
    include: [
      {
        model: Reservation,
        include: [
          { model: Participation, include: [{ model: User }] },
          {
            model: Timeslot,
            include: [
              {
                model: Sportfield,
                include: [
                  {
                    model: Company
                  }
                ]
              }
            ]
          },
          {
            model: User
          }
        ]
      }
    ]
  })
    .then(participations => {
      participations.sort((poA, poB) => {
        return (
          poA.reservation.timeslot.startTime <=
          poB.reservation.timeslot.startTime
        );
      });

      res.status(200).json(participations);
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.putCreate = (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    const error = new Error("Validation failed.");
    error.statusCode = 422;
    error.data = errors.array();
    throw error;
  }

  const timeslotId = req.body.timeslotId;
  const users = req.body.users;
  let savedTimeslot;

  Timeslot.findByPk(timeslotId, {
    include: [
      {
        model: Reservation
      }
    ]
  })
    .then(timeslot => {
      if (!timeslot) {
        const error = new Error("Timeslot does not exist.");
        error.statusCode = 500;
        throw error;
      }

      if (!!timeslot.toJSON().reservations.length) {
        const error = new Error("Timeslot has already been reserved.");
        error.statusCode = 403;
        throw error;
      }

      savedTimeslot = timeslot;
      return User.findByPk(req.userId);
    })
    .then(user => {
      if (!user) {
        const error = new Error("User does not exist.");
        error.statusCode = 500;
        throw error;
      }

      return Reservation.create({
        userId: user.id,
        timeslotId: savedTimeslot.id
      });
    })
    .then(reservation => {
      if (!reservation) {
        const error = new Error("Reservation could not be created.");
        error.statusCode = 500;
        throw error;
      }
      res.status(201).json({
        message: "Reservation added successfully!",
        reservation
      });

      users.push(req.userId);
      users.forEach(userId => {
        Participation.create({
          userId: userId,
          reservationId: reservation.id
        });
      });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};
