const Timeslot = require("../models/timeslot");
const Poll = require("../models/poll");
const User = require("../models/user");
const Vote = require("../models/vote");
const Polloption = require("../models/polloption");
const Company = require("../models/company");
const Sportfield = require("../models/sportfield");
const Reservation = require("../models/reservation");
const { validationResult } = require("express-validator/check");

exports.putVote = (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    const error = new Error("Validation failed.");
    error.statusCode = 422;
    error.data = errors.array();
    throw error;
  }

  const userId = req.userId;
  const pollOptionId = req.params.id;

  Vote.create({ userId: userId, polloptionId: pollOptionId })
    .then(vote => {
      res.status(201).json({
        message: "Vote created successfully!",
        vote
      });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};
