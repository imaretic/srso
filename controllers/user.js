const User = require("../models/user");
const Company = require("../models/company");
const { validationResult } = require("express-validator/check");

/**
 * Set user as admin or remove admin role.
 */
exports.postToggleAdmin = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error("Validation failed.");
    error.statusCode = 422;
    error.data = errors.array();
    throw error;
  }

  const userId = req.body.userId;

  User.findByPk(userId)
    .then(user => {
      if (!user) {
        const error = new Error("User not found.");
        error.statusCode = 500;
        throw error;
      }
      return user.update({ isAdmin: !user.isAdmin });
    })
    .then(user => {
      res.status(200).json({ message: "User updated", user: user });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

/**
 * Check if user with certain email exists.
 */
exports.postCheckEmail = (req, res, next) => {
  const email = req.body.email;

  User.findOne({ where: { email: email } })
    .then(user => {
      if (!user) {
        res.status(404).json({ message: "User does not exist" });
      } else {
        res.status(200).json({ message: "User does exist", id: user.id });
      }
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

/**
 * Returns all users.
 */
exports.getUsers = (req, res, next) => {
  User.findAll()
    .then(users => {
      res.status(200).json(users);
    })
    .catch(err => {
      console.log(err);
    });
};

/**
 * Returns specific user.
 */
exports.getUser = (req, res, next) => {
  const userId = req.params.id;

  if (userId != req.userId) {
    const error = new Error("Not authenticated.");
    error.statusCode = 401;
    throw error;
  }

  User.findByPk(userId, {
    include: [
      {
        model: Company
      }
    ]
  })
    .then(user => {
      if (!user) {
        const error = new Error("User not found.");
        error.statusCode = 400;
        throw error;
      }
      res.json(user);
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.deleteUser = (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    const error = new Error("Validation failed.");
    error.statusCode = 422;
    error.data = errors.array();
    throw error;
  }

  const userId = req.params.id;

  User.findByPk(userId)
    .then(user => {
      if (!user) {
        const error = new Error("User not found.");
        error.statusCode = 500;
        throw error;
      }
      return user.destroy();
    })
    .then(status => {
      return res.status(200).json({
        message: "User successfully deleted!"
      });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};
