const Timeslot = require("../models/timeslot");
const Poll = require("../models/poll");
const User = require("../models/user");
const Vote = require("../models/vote");
const Polloption = require("../models/polloption");
const Company = require("../models/company");
const Sportfield = require("../models/sportfield");
const Reservation = require("../models/reservation");
const { validationResult } = require("express-validator/check");

exports.getByUser = (req, res, next) => {
  const userId = req.params.id;

  Poll.findAll({ where: { userId: userId } })
    .then(polls => {
      res.status(200).json(polls);
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.getPoll = (req, res, next) => {
  const pollId = req.params.id;

  /**
   * Rastavit skroz na rucne upite...
   */

  Poll.findByPk(pollId, {
    include: [
      {
        model: User
      },
      {
        model: Polloption,
        include: [
          {
            model: Vote
          },
          {
            model: Timeslot,
            include: [{ model: Sportfield, include: [{ model: Company }] }]
          }
        ]
      }
    ]
  })
    .then(poll => {
      poll.polloptions.sort((poA, poB) => {
        return poA.timeslot.startTime > poB.timeslot.startTime;
      });

      res.status(200).json(poll);
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.putCreate = (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    const error = new Error("Validation failed.");
    error.statusCode = 422;
    error.data = errors.array();
    throw error;
  }

  const userId = req.userId;
  const timeslots = req.body.timeslots;

  Poll.create({ userId: userId })
    .then(poll => {
      timeslots.forEach(timeslotId => {
        Polloption.create({ pollId: poll.id, timeslotId: timeslotId });
      });

      res.status(201).json({
        message: "Poll created successfully!",
        poll
      });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};
