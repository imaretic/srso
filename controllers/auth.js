const User = require("../models/user");
const Company = require("../models/company");
const { validationResult } = require("express-validator/check");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

module.exports.login = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error("Validation failed.");
    error.statusCode = 422;
    error.data = errors.array();
    throw error;
  }

  const email = req.body.email;
  const password = req.body.password;
  let loadedUser;

  User.findOne({
    where: { email: email },
    include: [
      {
        model: Company
      }
    ]
  })
    .then(user => {
      if (!user) {
        const error = new Error("Invalid email or password.");
        error.statusCode = 422;
        throw error;
      }
      loadedUser = user;
      return bcrypt.compare(password, user.password);
    })
    .then(isEqual => {
      if (!isEqual) {
        /**
         * Send same message for security reasons.
         */
        const error = new Error("Invalid email or password.");
        error.statusCode = 422;
        throw error;
      }
      const token = jwt.sign({ userId: loadedUser.id }, "secretkey", {
        expiresIn: "6h"
      });
      res
        .status(200)
        .json({ message: "Logged in!", token: token, user: loadedUser });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

module.exports.signup = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error("Validation failed.");
    error.statusCode = 422;
    error.data = errors.array();
    throw error;
  }

  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const password = req.body.password;
  const phone = req.body.phone;
  const country = req.body.country;
  let isAdmin = false;

  /**
   * Check if database is empty.
   * If it's empty, set user's admin permission to TRUE.
   */
  User.findAll()
    .then(users => {
      if (users.length === 0) {
        isAdmin = true;
      }
      return bcrypt.hash(password, 12);
    })
    .then(hashedPw => {
      return User.create({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: hashedPw,
        phone: phone,
        country: country,
        isAdmin: isAdmin
      });
    })
    .then(user => {
      res.status(201).json({ message: "User created!", user });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

module.exports.edit = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error("Validation failed.");
    error.statusCode = 422;
    error.data = errors.array();
    throw error;
  }

  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const password = req.body.password;
  const phone = req.body.phone;
  const country = req.body.country;

  bcrypt
    .hash(password, 12)
    .then(hashedPw => {
      return User.findByPk(req.userId).then(user => {
        if (!user) {
          const error = new Error("User does not exist.");
          error.statusCode = 500;
          throw error;
        }
        return user.update({
          firstName: firstName,
          lastName: lastName,
          email: email,
          password: hashedPw,
          phone: phone,
          country: country
        });
      });
    })
    .then(user => {
      res.status(200).json({ message: "User updated!", user });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};
