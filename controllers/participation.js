const Participation = require("../models/participation");
const Reservation = require("../models/reservation");
const Timeslot = require("../models/timeslot");
const stripe = require("stripe")("sk_test_3Fq6pieyyeOGOg3aNN5vLQss");

exports.postPay = (req, res, next) => {
  const token = req.body.token;
  const participationId = req.params.id;
  let savedParticipation;
  let numberOfParticipations;

  Participation.findByPk(participationId)
    .then(participation => {
      if (!participation) {
        const error = new Error("Participation does not exist.");
        error.statusCode = 500;
        throw error;
      }
      savedParticipation = participation;
      // Participation does exist, continue with payment

      return Participation.findAll({
        where: { reservationId: savedParticipation.reservationId }
      });
    })
    .then(otherParticipations => {
      numberOfParticipations = otherParticipations.length;
      return Reservation.findByPk(savedParticipation.reservationId);
    })
    .then(reservation => {
      return Timeslot.findByPk(reservation.timeslotId);
    })
    .then(timeslot => {
      const amount = (100 * timeslot.price) / numberOfParticipations;
      return stripe.charges.create({
        amount: amount,
        currency: "HRK",
        description: "Participation_" + participationId,
        source: token
      });
    })

    .then(data => {
      savedParticipation.update({ paid: true });
      res.status(200).json({ message: data.status });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};
