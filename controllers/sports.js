const Sport = require("../models/sport");

exports.getSports = (req, res, next) => {
  Sport.findAll()
    .then(sports => {
      res.status(200).json(sports);
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.putCreate = (req, res, next) => {
  const name = req.body.name;
  const image = req.file;
  const imageUrl = req.file.path.replace("\\", "/");

  Sport.create({ name: name, imageUrl: imageUrl })
    .then(sport => {
      res.status(201).json({ message: "Sport added successfully!", sport });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.deleteSport = (req, res, next) => {
  const sportId = req.params.id;
  Sport.findByPk(sportId)
    .then(sport => {
      return sport.destroy();
    })
    .then(() => {
      res.status(200).json({ message: "Sport deleted successfully!" });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};
