const Timeslot = require("../models/timeslot");
const Company = require("../models/company");
const Sportfield = require("../models/sportfield");
const Reservation = require("../models/reservation");
const moment = require("moment");
const { validationResult } = require("express-validator/check");

/**
 * Return specific timeslots by array of timeslot IDs.
 */
exports.getTimeslotsByList = (req, res, next) => {
  const timeslots = req.body.timeslots;

  Timeslot.findAll({
    order: [["startTime", "ASC"]],
    where: {
      id: timeslots
    },
    include: [
      {
        model: Reservation
      },
      {
        model: Sportfield,
        include: [{ model: Company }]
      }
    ]
  })
    .then(timeslots => {
      res.status(200).json(timeslots);
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.getTimeslots = (req, res, next) => {
  Timeslot.findAll({
    order: [["startTime", "ASC"]],
    include: [
      {
        model: Reservation
      }
    ]
  })
    .then(timeslots => {
      res.status(200).json(timeslots);
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

/**
 * Get specific timeslot.
 * /timeslots/:id?extra=true
 * If extra is true then return information about sportfield and company.
 */
exports.getTimeslot = (req, res, next) => {
  const timeslotId = req.params.id;
  const extra = req.query.extra;
  let savedTimeslot = null,
    savedSportfield = null;

  Timeslot.findByPk(timeslotId, {
    include: [
      {
        model: Reservation
      }
    ],
    attributes: { exclude: ["createdAt", "updatedAt"] }
  })
    .then(timeslot => {
      if (!timeslot) {
        const error = new Error("Timeslot does not exist.");
        error.statusCode = 500;
        throw error;
      }
      if (extra) {
        savedTimeslot = timeslot;
        return Sportfield.findByPk(timeslot.sportfieldId, {
          attributes: { exclude: ["createdAt", "updatedAt"] }
        });
      }
      res.status(200).json(timeslot);
    })
    .then(sportfield => {
      if (sportfield) {
        savedSportfield = sportfield;
        return Company.findByPk(sportfield.companyId, {
          attributes: { exclude: ["createdAt", "updatedAt", "userId"] }
        });
      }
      res.status(200).json({
        timeslot: savedTimeslot,
        sportfield: null,
        company: null
      });
    })
    .then(company => {
      const { companyId, ...returnSportfield } = savedSportfield.dataValues;
      const { sportfieldId, ...returnTimeslot } = savedTimeslot.dataValues;
      res.status(200).json({
        ...returnTimeslot,
        company: { ...company.dataValues },
        sportfield: { ...returnSportfield }
      });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

/**
 * Get timeslots by date and sportfield.
 * /timeslots/bydate
 */
exports.postByDate = (req, res, next) => {
  const date = moment(req.body.date).format("YYYY-MM-DD");
  const sportfieldId = req.body.sportfieldId;

  Timeslot.findAll({
    order: [["startTime", "ASC"]],
    where: {
      sportfieldId: sportfieldId
    },
    include: [
      {
        model: Reservation
      }
    ]
  })
    .then(timeslots => {
      const filteredTimeslots = timeslots.filter(timeslot => {
        const databaseDate = moment(timeslot.startTime);
        const databaseYMD = databaseDate.format("YYYY-MM-DD");
        if (date === databaseYMD) {
          // Check minutes
          if (databaseDate > moment()) {
            return true;
          }
        } else return false;
      });

      res.status(200).json(filteredTimeslots);
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.putCreate = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error("Validation failed.");
    error.statusCode = 422;
    error.data = errors.array();
    throw error;
  }

  const {
    numberOfTimeslots,
    startTime,
    timeslotDuration,
    price,
    sportfieldId
  } = req.body;
  const pTimeslotDuration = parseInt(timeslotDuration);

  let timeslots = [];
  for (let i = 0; i < numberOfTimeslots; i++) {
    const convertedStartTime = moment(startTime)
      .add(i * pTimeslotDuration, "hours")
      .format();
    const convertedEndTime = moment(startTime)
      .add(i * pTimeslotDuration + pTimeslotDuration, "hours")
      .format();
    timeslots.push({
      startTime: convertedStartTime,
      endTime: convertedEndTime,
      price: price,
      sportfieldId
    });
  }

  Timeslot.bulkCreate(timeslots)
    .then(() => {
      res.status(201).json({
        message: "Timeslots added successfully!"
      });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.deleteTimeslot = (req, res, next) => {
  const timeslotId = req.params.id;
  Timeslot.findByPk(timeslotId)
    .then(timeslot => {
      return timeslot.destroy();
    })
    .then(() => {
      res.status(200).json({ message: "Timeslot deleted successfully!" });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};
