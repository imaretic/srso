# System for reservation of sport objects

Frontend part of the final work project.
To run this React project, run: `npm instal` and `npm start`.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Code Samples

### Notifications

Required imports:

```javascript
import { connect } from "react-redux";
import { setNotification } from "../../../actions/notification";
```

Required export wrapper:

```javascript
export default connect()(ComponentName);
```

Methods:

```javascript
this.props.dispatch(
  setNotification({
    message: "Termin uspješno rezerviran!",
    error: false
  })
);
```
