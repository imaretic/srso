// SET_USER
export const setUser = (user = {}) => ({
  type: "SET_USER",
  user: user
});

// REMOVE_USER
export const removeUser = () => ({
  type: "REMOVE_USER"
});
