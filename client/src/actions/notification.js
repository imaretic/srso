// SET_NOTIFICATION
export const setNotification = (notification = {}) => ({
  type: "SET_NOTIFICATION",
  notification: notification
});

// REMOVE_NOTIFICATION
export const removeNotification = () => ({
  type: "REMOVE_NOTIFICATION"
});
