import React from "react";
import { connect } from "react-redux";
import AppRouter from "./routers/AppRouter";
import Notification from "./components/common/util/Notification";
import { removeNotification } from "./actions/notification";
import { CSSTransition } from "react-transition-group";

class App extends React.Component {
  componentDidUpdate = () => {
    if (this.props.notification.visible) {
      setTimeout(() => {
        this.props.dispatch(removeNotification());
      }, 3000);
    }
  };

  render() {
    return (
      <React.Fragment>
        <CSSTransition
          in={this.props.notification.visible}
          timeout={600}
          classNames="notification"
          unmountOnExit
        >
          <Notification
            message={this.props.notification.message}
            error={this.props.notification.error}
          />
        </CSSTransition>
        <AppRouter />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    notification: state.notification
  };
};

export default connect(mapStateToProps)(App);
