import React from "react";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";

export const PrivateRoute = ({
  isAuthenticated,
  component: Component,
  reloadUser,
  ...rest
}) => (
  <Route
    {...rest}
    component={props =>
      isAuthenticated ? (
        <Redirect to="/" />
      ) : (
        <Component {...props} reloadUser={reloadUser} />
      )
    }
  />
);

const mapStateToProps = state => ({
  isAuthenticated: !!state.user.id
});

export default connect(mapStateToProps)(PrivateRoute);
