import React from "react";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";

export const AdminRouter = ({
  isAuthenticated,
  component: Component,
  ...other
}) => (
  <Route
    {...other}
    component={props =>
      isAuthenticated ? <Component {...props} /> : <Redirect to="/" />
    }
  />
);

const mapStateToProps = state => ({
  isAuthenticated: state.user.isAdmin
});

export default connect(mapStateToProps)(AdminRouter);
