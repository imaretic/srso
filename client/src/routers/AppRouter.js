import React from "react";
import HomePage from "../components/pages/HomePage";
import LoginPage from "../components/pages/LoginPage";
import RegisterPage from "../components/pages/RegisterPage";
import ProfilePage from "../components/pages/ProfilePage";
import CompanyPage from "../components/pages/CompanyPage";
import FieldPage from "../components/pages/FieldPage";
import ReservePage from "../components/pages/ReservePage";
import MyCompanyPage from "../components/pages/MyCompanyPage";
import MyPollsPage from "../components/pages/MyPollsPage";
import NewPollPage from "../components/pages/NewPollPage";
import PollPage from "../components/pages/PollPage";
import MyCompanyCreatePage from "../components/pages/MyCompanyCreatePage";
import MyCompanyFieldPage from "../components/pages/MyCompanyFieldPage";
import MyReservationsPage from "../components/pages/MyReservationsPage";
import AboutPage from "../components/pages/AboutPage";
import SportsPage from "../components/pages/SportsPage";
import NotFoundPage from "../components/pages/NotFoundPage";
import UsersPage from "../components/pages/UsersPage";
import Nav from "../components/common/nav/Nav";
import Footer from "../components/common/Footer";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { connect } from "react-redux";
import PrivateRouter from "./PrivateRouter";
import AdminRouter from "./AdminRouter";
import PublicRouter from "./PublicRouter";
import { setUser, removeUser } from "../actions/user";
import PageLoader from "../components/common/loader/PageLoader";
import ScrollToTop from "./ScrollToTop";

class AppRouter extends React.Component {
  state = {
    loaded: false
  };

  componentDidMount = () => {
    this.loadUser();
  };

  loadUser = () => {
    const token = localStorage.getItem("token");
    const userId = localStorage.getItem("userId");

    if (!token || !userId) {
      this.props.dispatch(removeUser());
      this.setState(() => ({
        loaded: true
      }));
      return;
    }

    fetch(`/api/users/${userId}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => {
        if (res.status !== 200) {
          const error = new Error("Pogreška pri dohvatu podataka.");
          error.statusCode = res.status;
          throw error;
        }
        return res.json();
      })
      .then(resData => {
        this.props.dispatch(setUser(resData));
        this.setState(() => ({
          loaded: true
        }));
      })
      .catch(err => {
        this.props.dispatch(removeUser());
        this.setState(() => ({
          loaded: true
        }));
      });
  };

  render() {
    if (!this.state.loaded) return <PageLoader />;

    return (
      <Router>
        <Nav />

        <ScrollToTop>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <PublicRouter
              path="/login"
              component={LoginPage}
              reloadUser={this.loadUser}
            />
            <PublicRouter
              path="/register"
              component={RegisterPage}
              reloadUser={this.loadUser}
            />
            <PrivateRouter
              path="/profile"
              component={ProfilePage}
              reloadUser={this.loadUser}
            />
            <PrivateRouter
              path="/mycompany/field/:id"
              component={MyCompanyFieldPage}
            />
            <PrivateRouter
              path="/mycompany/create"
              component={MyCompanyCreatePage}
              reloadUser={this.loadUser}
            />
            <PrivateRouter
              path="/mycompany"
              component={MyCompanyPage}
              reloadUser={this.loadUser}
            />
            <PrivateRouter path="/mypolls" component={MyPollsPage} />
            <PrivateRouter path="/newpoll" component={NewPollPage} />
            <PrivateRouter path="/poll/:id" component={PollPage} />
            <AdminRouter path="/users" component={UsersPage} />
            <Route path="/company/:id/" component={CompanyPage} />
            <Route path="/reserve/:timeslotId" component={ReservePage} />
            <PrivateRouter
              path="/myreservations/"
              component={MyReservationsPage}
            />
            <Route path="/field/:id" component={FieldPage} />
            <Route path="/about" component={AboutPage} />
            <Route path="/sports" component={SportsPage} />
            <Route component={NotFoundPage} />
          </Switch>
        </ScrollToTop>
        <Footer />
      </Router>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(AppRouter);
