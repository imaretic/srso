const notificationReducerDefaultState = {
  visible: false
};

export default (state = notificationReducerDefaultState, action) => {
  switch (action.type) {
    case "SET_NOTIFICATION":
      return { ...action.notification, visible: true };
    case "REMOVE_NOTIFICATION":
      return { ...state, visible: false };
    default:
      return state;
  }
};
