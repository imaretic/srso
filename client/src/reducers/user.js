const userReducerDefaultState = {};

export default (state = userReducerDefaultState, action) => {
  switch (action.type) {
    case "SET_USER":
      return action.user;
    case "REMOVE_USER":
      localStorage.removeItem("userId");
      localStorage.removeItem("token");
      return userReducerDefaultState;
    default:
      return state;
  }
};
