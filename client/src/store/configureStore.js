import { createStore, combineReducers } from "redux";
import userReducer from "../reducers/user";
import notificationReducer from "../reducers/notification";

export default () => {
  const store = createStore(
    combineReducers({
      user: userReducer,
      notification: notificationReducer
    }),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  );

  return store;
};
