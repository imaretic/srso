import React from "react";

export default props => {
  return (
    <ul className="nav__list__dropdown">
      <div className="nav__list__dropdown__spacing" />
      <div className="nav__list__dropdown__corner">
        <div />
      </div>
      <div className="nav__list__dropdown__shaddow">{props.children}</div>
    </ul>
  );
};
