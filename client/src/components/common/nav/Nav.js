import React from "react";
import { Link, NavLink, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { CSSTransition } from "react-transition-group";
import logo from "../../../img/logo.svg";
import BtnGreenSmall from "../btns/BtnGreenSmall";
import { removeUser } from "../../../actions/user";
import avatar from "../../../img/avatar.svg";
import Dropdown from "./Dropdown";
import DropdownItem from "./DropdownItem";
import company from "../../../img/nav/company.svg";
import settings from "../../../img/nav/settings.svg";
import logout from "../../../img/nav/logout.svg";

class Nav extends React.Component {
  state = {
    profileDropdown: false
  };

  handleLogout = e => {
    localStorage.removeItem("token");
    localStorage.removeItem("userId");
    this.props.dispatch(removeUser());
    this.setState(() => ({ profileDropdown: false }));
    this.props.history.push("/login");
  };

  handleProfileDropdownIn = e => {
    this.setState(() => ({ profileDropdown: true }));
  };

  handleProfileDropdownOut = e => {
    this.setState(() => ({ profileDropdown: false }));
  };

  render() {
    return (
      <nav>
        <div className="container">
          <div className="nav__logo">
            <NavLink to="/">
              <img src={logo} alt="Logo" />
              <span>Rezerviraj termin online</span>
            </NavLink>
          </div>
          <ul className="nav__list">
            {this.props.loggedIn && this.props.user.isAdmin && (
              <React.Fragment>
                <li className="nav__list__item">
                  <NavLink className="nav__list__item__content" to="/users">
                    Korisnici
                  </NavLink>
                </li>
                <li className="nav__list__item">
                  <NavLink className="nav__list__item__content" to="/sports">
                    Sportovi
                  </NavLink>
                </li>
              </React.Fragment>
            )}
            {this.props.user.id ? (
              <React.Fragment>
                <li className="nav__list__item">
                  <NavLink
                    className="nav__list__item__content"
                    to="/myreservations"
                  >
                    Rezervacije
                  </NavLink>
                </li>
                <li className="nav__list__item">
                  <NavLink className="nav__list__item__content" to="/mypolls">
                    Ankete
                  </NavLink>
                </li>
                <li className="nav__list__item">
                  <NavLink className="nav__list__item__content" to="/newpoll">
                    Nova anketa
                  </NavLink>
                </li>
                <li
                  className="nav__list__item nav__list__item--avatar-container"
                  onMouseEnter={this.handleProfileDropdownIn}
                  onMouseLeave={this.handleProfileDropdownOut}
                >
                  <button className="nav__list__item__content">
                    <div className="nav__list__item__content__text">
                      {`${this.props.user.firstName} ${
                        this.props.user.lastName
                      }`}
                    </div>
                    <div className="nav__avatar__wrapper">
                      <img className="nav__avatar" alt="Avatar" src={avatar} />
                    </div>
                    <div className="nav__list__item__content_arrow" />
                  </button>
                  <CSSTransition
                    in={this.state.profileDropdown}
                    timeout={200}
                    classNames="dd"
                    unmountOnExit
                  >
                    <Dropdown>
                      <DropdownItem
                        label="Moja tvrtka"
                        icon={company}
                        line={true}
                        path="/mycompany"
                      />
                      <DropdownItem
                        label="Profil"
                        icon={settings}
                        path="/profile"
                      />
                      <DropdownItem
                        label="Odjava"
                        icon={logout}
                        green={true}
                        handleClick={this.handleLogout}
                      />
                    </Dropdown>
                  </CSSTransition>
                </li>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <li className="nav__list__item">
                  <NavLink className="nav__list__item__content" to="/login">
                    Prijava
                  </NavLink>
                </li>
                <li className="nav__list__item nav__list__item--btn-container">
                  <Link className="nav__list__item__content" to="/register">
                    <BtnGreenSmall label="Registracija" />
                  </Link>
                </li>
              </React.Fragment>
            )}
          </ul>
        </div>
      </nav>
    );
  }
}

const mapStateToProps = state => {
  return {
    loggedIn: !!state.user,
    user: state.user
  };
};

export default connect(mapStateToProps)(withRouter(Nav));
