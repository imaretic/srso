import React from "react";
import { NavLink } from "react-router-dom";

class DropdownItem extends React.Component {
  wrapLinkTag = jsx => {
    if (this.props.path)
      return (
        <NavLink
          to={this.props.path}
          className="link-element"
          onClick={this.props.handleClick}
        >
          {jsx}
        </NavLink>
      );
    return (
      <div onClick={this.props.handleClick} className="link-element">
        {jsx}
      </div>
    );
  };

  render() {
    const divStyle = {
      backgroundImage: "url(" + this.props.icon + ")"
    };

    return (
      <li
        className={
          "nav__list__dropdown__item " +
          (this.props.green ? `nav__list__dropdown__item--green` : "")
        }
      >
        {this.wrapLinkTag(
          <React.Fragment>
            <div className="nav__list__dropdown__item__text">
              {this.props.label}
            </div>
            <div
              alt="Company"
              style={divStyle}
              className="nav__list__dropdown__item__icon"
            />
            {this.props.line ? (
              <div className="nav__list__dropdown__item__line">
                <div />
              </div>
            ) : (
              ""
            )}
          </React.Fragment>
        )}
      </li>
    );
  }
}

export default DropdownItem;
