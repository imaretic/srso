import React from "react";
import { NavLink } from "react-router-dom";

class BtnGreenSmallWide extends React.Component {
  render() {
    if (this.props.to) {
      return (
        <NavLink
          to={this.props.to}
          className="btn btn--green btn--green-s btn--green-s--wide"
        >
          {this.props.label}
        </NavLink>
      );
    }

    return (
      <button
        className="btn btn--green btn--green-s btn--green-s--wide"
        type={this.props.submit ? "submit" : "button"}
      >
        <div className="btn__label">{this.props.label}</div>
      </button>
    );
  }
}

export default BtnGreenSmallWide;
