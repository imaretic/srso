import React from "react";
import LoaderBtnGreen from "../loader/LoaderBtnGreen";

class BtnGreen extends React.Component {
  render() {
    return (
      <button
        className="btn btn--green btn--green-m"
        disabled={this.props.loading || this.props.disabled}
        type={this.props.submit ? "submit" : "button"}
        onClick={this.props.onClick}
      >
        {this.props.loading && <LoaderBtnGreen />}
        <div className="btn__label">{this.props.label}</div>
      </button>
    );
  }
}

export default BtnGreen;
