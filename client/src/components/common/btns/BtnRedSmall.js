import React from "react";
import LoaderBtnRed from "../loader/LoaderBtnRed";

class BtnRedSmall extends React.Component {
  render() {
    return (
      <button
        className="btn btn--red btn--red-s"
        disabled={this.props.loading}
        type={this.props.submit ? "submit" : "button"}
        onClick={this.props.onClick}
      >
        {this.props.loading && <LoaderBtnRed />}
        <div className="btn__label">{this.props.label}</div>
      </button>
    );
  }
}

export default BtnRedSmall;
