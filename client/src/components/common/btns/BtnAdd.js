import React from "react";

class BtnAdd extends React.Component {
  render() {
    return <button {...this.props} className="btn btn--green btn--add" />;
  }
}

export default BtnAdd;
