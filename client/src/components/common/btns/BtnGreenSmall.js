import React from "react";
import { NavLink } from "react-router-dom";
import LoaderBtnGreen from "../loader/LoaderBtnGreen";

class BtnGreenSmall extends React.Component {
  render() {
    if (this.props.to) {
      return (
        <NavLink to={this.props.to} className="btn btn--green btn--green-s">
          {this.props.label}
        </NavLink>
      );
    }

    return (
      <button
        className="btn btn--green btn--green-s"
        disabled={this.props.loading || this.props.disabled}
        type={this.props.submit ? "submit" : "button"}
        onClick={this.props.onClick}
      >
        {this.props.loading && <LoaderBtnGreen />}
        <div className="btn__label">{this.props.label}</div>
      </button>
    );
  }
}

export default BtnGreenSmall;
