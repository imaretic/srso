import React from "react";

class BtnDelete extends React.Component {
  render() {
    return <button {...this.props} className="btn btn--red btn--delete" />;
  }
}

export default BtnDelete;
