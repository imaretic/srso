import React from "react";
import Loader from "./Loader";

class SectionLoader extends React.Component {
  render() {
    return <Loader className="loader--green loader--section" />;
  }
}

export default SectionLoader;
