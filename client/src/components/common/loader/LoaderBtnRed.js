import React from "react";
import Loader from "./Loader";

class LoaderBtnRed extends React.Component {
  render() {
    return <Loader className="loader--light-red loader--btn" />;
  }
}

export default LoaderBtnRed;
