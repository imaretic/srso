import React from "react";
import Loader from "./Loader";

class PageLoader extends React.Component {
  render() {
    return (
      <div className="loader-page-wrapper">
        <Loader className="loader--green loader--page" />
      </div>
    );
  }
}

export default PageLoader;
