import React from "react";
import Loader from "./Loader";

class LoaderBtnGreen extends React.Component {
  render() {
    return <Loader className="loader--light-green loader--btn" />;
  }
}

export default LoaderBtnGreen;
