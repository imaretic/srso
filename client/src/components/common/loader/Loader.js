import React from "react";

class Loader extends React.Component {
  render() {
    return (
      <div className={"lds-ring " + this.props.className}>
        <div />
        <div />
        <div />
        <div />
      </div>
    );
  }
}

export default Loader;
