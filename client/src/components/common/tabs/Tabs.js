import React from "react";

export default class Tabs extends React.Component {
  state = {
    activeTab: 0
  };

  render() {
    return (
      <div className="tabs-area">
        <div className="tabs__header">
          {this.props.content.map((tab, index) => {
            return (
              <button
                key={index}
                className={
                  "tab " + (this.state.activeTab === index ? "tab--active" : "")
                }
                onClick={e => {
                  this.setState(() => {
                    return { activeTab: index };
                  });
                }}
              >
                {tab.label}
              </button>
            );
          })}
        </div>
        <div className="tabs__content">
          {this.props.content.map((tab, index) => {
            if (index === this.state.activeTab) {
              return tab.content(index);
            }
            return undefined;
          })}
        </div>
      </div>
    );
  }
}
