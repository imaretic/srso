import React from "react";

class FieldValidator extends React.Component {
  validate(fieldName, value) {
    let message = "";
    switch (fieldName) {
      case "name":
        if (!(value.length >= 3)) message = "Ime nije ispravno!";
        break;
      case "sport":
        if (!(value.length >= 1)) message = "Sport nije ispravan!";
        break;
      case "numberOfPlayers":
        if (!value.match(/^[0-9]*$/)) message = "Broj igrača nije ispravan!";
        break;
      case "conditions":
        if (!(value.length >= 3)) message = "Uvjeti nisu ispravni!";
        break;
      default:
        break;
    }
    return message;
  }

  render() {
    const children = React.Children.map(this.props.children, child => {
      return React.cloneElement(child, {
        validate: this.validate
      });
    });

    return children;
  }
}

export default FieldValidator;
