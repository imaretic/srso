import React from "react";

class SportValidator extends React.Component {
  validate(fieldName, value) {
    let message = "";
    switch (fieldName) {
      case "name":
        if (!(value.length >= 2)) message = "Ime nije ispravno!";
        break;
      default:
        break;
    }
    return message;
  }

  render() {
    const children = React.Children.map(this.props.children, child => {
      return React.cloneElement(child, {
        validate: this.validate
      });
    });

    return children;
  }
}

export default SportValidator;
