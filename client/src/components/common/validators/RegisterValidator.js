import React from "react";

class RegisterValidator extends React.Component {
  validate(fieldName, value) {
    let message = "";
    switch (fieldName) {
      case "email":
        if (!value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i))
          message = "Email nije ispravan!";
        break;
      case "password":
        if (!(value.length >= 2)) message = "Lozinka nije ispravna!";
        break;
      case "firstName":
        if (!(value.length >= 2)) message = "Ime nije ispravno!";
        break;
      case "lastName":
        if (!(value.length >= 2)) message = "Prezime nije ispravno!";
        break;
      case "phone":
        if (!value.match(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s./0-9]*$/))
          message = "Broj mobitela nije ispravan!";
        break;
      case "country":
        if (!(value.length >= 2)) message = "Država nije ispravna!";
        break;
      default:
        break;
    }
    return message;
  }

  render() {
    const children = React.Children.map(this.props.children, child => {
      return React.cloneElement(child, {
        validate: this.validate
      });
    });

    return children;
  }
}

export default RegisterValidator;
