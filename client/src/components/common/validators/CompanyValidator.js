import React from "react";

class CompanyValidator extends React.Component {
  validate(fieldName, value) {
    let message = "";
    switch (fieldName) {
      case "name":
        if (!(value.length >= 3)) message = "Ime nije ispravno!";
        break;
      case "email":
        if (!value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i))
          message = "Email nije ispravan!";
        break;
      case "phone":
        if (!value.match(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s./0-9]*$/))
          message = "Broj mobitela nije ispravan!";
        break;
      case "street":
        if (!(value.length >= 3)) message = "Ulica nije ispravna!";
        break;
      case "streetNumber":
        if (!(value.length >= 1)) message = "Kućni broj nije ispravan!";
        break;
      case "city":
        if (!(value.length >= 3)) message = "Grad nije ispravan!";
        break;
      case "postCode":
        if (!(value.length >= 3)) message = "Poštanski broj nije ispravan!";
        break;
      case "country":
        if (!(value.length >= 3)) message = "Država nije ispravna!";
        break;
      default:
        break;
    }
    return message;
  }

  render() {
    const children = React.Children.map(this.props.children, child => {
      return React.cloneElement(child, {
        validate: this.validate
      });
    });

    return children;
  }
}

export default CompanyValidator;
