import React from "react";

class FieldValidator extends React.Component {
  validate(fieldName, value) {
    let message = "";
    switch (fieldName) {
      case "price":
        if (!value.match(/^\d+(,\d{3})*(\.\d{1,2})?$/))
          message = "Cijena je obavezna!";
        break;
      case "timeslotsStartTime":
        if (!(value.length >= 1)) message = "Početno vrijeme nije ispravno!";
        break;
      case "timeslotDuration":
        if (!(value.length >= 1)) message = "Trajanje nije ispravno!";
        break;
      case "numberOfTimeslots":
        if (!value.match(/^[0-9]*$/)) message = "Broj je obavezan!";
        break;
      default:
        break;
    }
    return message;
  }

  render() {
    const children = React.Children.map(this.props.children, child => {
      return React.cloneElement(child, {
        validate: this.validate
      });
    });

    return children;
  }
}

export default FieldValidator;
