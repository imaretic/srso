import React from "react";
import { NavLink } from "react-router-dom";

class Footer extends React.Component {
  render() {
    return (
      <footer id="site-footer">
        <div className="container">
          <ul className="footer-links">
            <li>
              <NavLink to="/about">O projektu</NavLink>
            </li>
            <li>
              <a href="mailto:ivan.maretic@live.com">Kontakt</a>
            </li>
          </ul>
          <div className="footer-note">
            Projekt nastao kao Završni rad na Fakultetu elektrotehnike i
            računarstva
          </div>
          <div className="footer-copyright">Ivan Maretić, 2019.</div>
        </div>
      </footer>
    );
  }
}

export default Footer;
