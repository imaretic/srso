import React from "react";
import { CountryDropdown } from "react-country-region-selector";
import { connect } from "react-redux";
import { setNotification } from "../../../actions/notification";
import { Redirect } from "react-router";
import BtnGreen from "../btns/BtnGreen";
import FormMessage from "../util/FormMessage";
import InputContainer from "../util/InputContainer";

class RegisterForm extends React.Component {
  state = {
    registered: false,
    loading: false,
    submitted: false,
    formError: false,
    formMessage: "",
    email: {
      value: "",
      error: "Email je obavezan!"
    },
    password: {
      value: "",
      error: "Lozinka je obavezna!"
    },
    firstName: {
      value: "",
      error: "Ime je obavezno!"
    },
    lastName: {
      value: "",
      error: "Prezime je obavezno!"
    },
    phone: {
      value: "",
      error: "Kontakt broj je obavezan!"
    },
    country: {
      value: "Croatia",
      error: ""
    }
  };

  validationFailed = () => {
    return (
      !!this.state.email.error ||
      !!this.state.password.error ||
      !!this.state.firstName.error ||
      !!this.state.lastName.error ||
      !!this.state.phone.error ||
      !!this.state.country.error
    );
  };

  handleUserInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => {
      return {
        [name]: {
          value: value,
          error: this.props.validate(name, value)
        }
      };
    });
  };

  onSubmit = e => {
    e.preventDefault();
    this.setState(prevState => {
      return { loading: !prevState.loading, submitted: true };
    });

    if (this.validationFailed()) {
      this.setState(prevState => {
        return { loading: false };
      });
      return;
    }

    fetch("/api/auth/signup", {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        firstName: this.state.firstName.value,
        lastName: this.state.lastName.value,
        phone: this.state.phone.value,
        country: this.state.country.value,
        email: this.state.email.value,
        password: this.state.password.value
      })
    })
      .then(res => {
        // Validation failed
        if (res.status === 422) {
          const error = new Error();
          error.res = res.json();
          error.statusCode = 422;
          throw error;
        }

        // Some other problem occured
        if (res.status !== 200 && res.status !== 201) {
          const error = new Error(
            "Nesporazum u komunikaciji s bazom. Molimo pokušajte ponovo."
          );
          error.statusCode = res.status;
          throw error;
        }

        return res.json();
      })
      .then(resData => {
        this.props.dispatch(
          setNotification({
            message: "Registracija uspješna!",
            error: false
          })
        );
        this.setState(() => ({
          registered: true
        }));
      })
      .catch(err => {
        if (err.statusCode === 422) {
          err.res.then(resData => {
            // Problem with fields
            resData.data.forEach(item => {
              this.setState(prevState => {
                return {
                  loading: false,
                  [item.param]: {
                    value: prevState[item.param].value,
                    error: item.msg
                  }
                };
              });
            });
          });
        } else {
          this.setState(() => {
            return {
              formError: true,
              formMessage: err.message,
              loading: false
            };
          });
        }
      });
  };

  selectCountry = val => {
    this.setState({ country: { value: val, error: "" } });
  };

  render() {
    if (this.state.registered) return <Redirect to="/login" />;

    return (
      <form onSubmit={this.onSubmit}>
        <InputContainer
          message={this.state.submitted && this.state.firstName.error}
          type="text"
          name="firstName"
          value={this.state.firstName.value}
          className={
            "inputFieldWhite " +
            (this.state.submitted &&
              this.state.firstName.error &&
              `inputFieldWhite--error`)
          }
          placeholder="Ime"
          onChange={this.handleUserInput}
          autoComplete="given-name"
        />

        <InputContainer
          message={this.state.submitted && this.state.lastName.error}
          type="text"
          name="lastName"
          value={this.state.lastName.value}
          className={
            "inputFieldWhite " +
            (this.state.submitted &&
              this.state.lastName.error &&
              `inputFieldWhite--error`)
          }
          placeholder="Prezime"
          onChange={this.handleUserInput}
          autoComplete="family-name"
        />

        <InputContainer
          message={this.state.submitted && this.state.email.error}
          type="email"
          name="email"
          value={this.state.email.value}
          className={
            "inputFieldWhite " +
            (this.state.submitted &&
              this.state.email.error &&
              `inputFieldWhite--error`)
          }
          placeholder="E-mail"
          onChange={this.handleUserInput}
          autoComplete="email"
        />

        <InputContainer
          message={this.state.submitted && this.state.password.error}
          type="password"
          name="password"
          value={this.state.password.value}
          className={
            "inputFieldWhite " +
            (this.state.submitted &&
              this.state.password.error &&
              `inputFieldWhite--error`)
          }
          placeholder="Lozinka"
          autoComplete="new-password"
          onChange={this.handleUserInput}
        />

        <InputContainer
          message={this.state.submitted && this.state.phone.error}
          type="tel"
          name="phone"
          value={this.state.phone.value}
          className={
            "inputFieldWhite " +
            (this.state.submitted &&
              this.state.phone.error &&
              `inputFieldWhite--error`)
          }
          placeholder="Broj mobitela"
          onChange={this.handleUserInput}
          autoComplete="phone"
        />

        <div className="input-container">
          <CountryDropdown
            value={this.state.country.value}
            className="inputFieldWhite"
            onChange={val => this.selectCountry(val)}
          />
        </div>

        {this.state.formMessage && (
          <FormMessage
            message={this.state.formMessage}
            error={this.state.formError}
          />
        )}
        <BtnGreen
          label={"Registracija"}
          submit={true}
          loading={this.state.loading}
        />
      </form>
    );
  }
}

export default connect()(RegisterForm);
