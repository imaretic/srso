import React from "react";
import { connect } from "react-redux";
import TimePicker from "react-times";
import "react-times/css/material/default.css";
import "react-times/css/classic/default.css";
import { setNotification } from "../../../actions/notification";
import InputContainer from "../util/InputContainer";
import InputObject from "../util/InputObject";
import BtnGreen from "../btns/BtnGreen";
import moment from "moment";
import "react-dates/initialize";
import { SingleDatePicker, isInclusivelyAfterDay } from "react-dates";
import "react-dates/lib/css/_datepicker.css";

class TimeslotForm extends React.Component {
  state = {
    selectedDate: moment(),
    focused: false,
    timeFocused: false,
    hour: 7,
    minute: 0,
    timeslotData: [],
    price: {
      value: "",
      error: "Cijena je obavezna!"
    },
    startTime: {
      error: "Vrijeme nije ispravno!"
    },
    timeslotDuration: {
      value: "1",
      error: ""
    },
    numberOfTimeslots: {
      value: "1",
      error: ""
    }
  };

  componentDidMount = () => {
    this.checkTimeAndDate();
  };

  validationFailed = () => {
    return (
      !!this.state.price.error ||
      !!this.state.startTime.error ||
      !!this.state.timeslotDuration.error ||
      !!this.state.numberOfTimeslots.error
    );
  };

  handleUserInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => {
      return {
        touched: true,
        [name]: {
          value: value,
          error: this.props.validate(name, value)
        }
      };
    });
  };

  onDateChange = selectedDate => {
    if (selectedDate) {
      this.setState(
        () => {
          return { selectedDate };
        },
        () => {
          this.checkTimeAndDate();
        }
      );
      this.props.onDateChange(selectedDate);
    }
  };

  onFocusChange = ({ focused }) => {
    this.setState(() => ({ focused }));
  };

  handleTimeChange = options => {
    const { hour, minute } = options;
    this.setState({ hour, minute }, () => {
      this.checkTimeAndDate();
    });
  };

  checkTimeAndDate = () => {
    const currentTime = moment();
    const selectedTime = moment().set({
      hour: this.state.hour,
      minute: this.state.minute
    });
    const stateDate = moment(this.state.selectedDate);

    if (stateDate.format("YYYY-MM-DD") === currentTime.format("YYYY-MM-DD")) {
      if (selectedTime > currentTime) {
        this.setState({ startTime: { error: "" } });
      } else {
        this.setState({ startTime: { error: "Vrijeme nije ispravno" } });
      }
    } else {
      this.setState({ startTime: { error: "" } });
    }
  };

  handleTimeFocusChange = timeFocused => {
    this.setState({ timeFocused });
  };

  onSubmit = e => {
    e.preventDefault();
    this.setState(prevState => {
      return {
        loading: !prevState.loading,
        submitted: true
      };
    });

    if (this.validationFailed()) {
      this.setState(prevState => {
        return { loading: false };
      });
      return;
    }

    const startTime = moment(this.state.selectedDate)
      .set({ hour: this.state.hour, minute: this.state.minute, seconds: 0 })
      .format();

    const token = localStorage.getItem("token");
    fetch(`/api/timeslots/create`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      },
      body: JSON.stringify({
        startTime,
        price: this.state.price.value,
        sportfieldId: this.props.id,
        numberOfTimeslots: this.state.numberOfTimeslots.value,
        timeslotDuration: this.state.timeslotDuration.value
      })
    })
      .then(res => {
        // Validation failed
        if (res.status === 422) {
          const error = new Error();
          error.res = res.json();
          error.statusCode = 422;
          throw error;
        }
        // Some other problem occured
        if (res.status !== 200 && res.status !== 201) {
          const error = new Error(
            "Nesporazum u komunikaciji s bazom. Molimo pokušajte ponovo."
          );
          error.statusCode = res.status;
          throw error;
        }

        return res.json();
      })
      .then(resData => {
        this.setState(() => ({
          loading: false
        }));
        this.props.dispatch(
          setNotification({
            visible: true,
            message: "Termin(i) uspješno dodani!",
            error: false
          })
        );
        this.props.onSuccess();
      })
      .catch(err => {
        if (err.statusCode === 422) {
          err.res.then(resData => {
            // Problem with fields
            resData.data.forEach(item => {
              this.setState(prevState => {
                return {
                  loading: false,
                  [item.param]: {
                    value: prevState[item.param].value,
                    error: item.msg
                  }
                };
              });
            });
          });
        } else {
          this.setState(() => {
            return {
              formError: true,
              formMessage: err.message,
              loading: false
            };
          });
        }
      });
  };

  render() {
    return (
      <div className="timeslot-form">
        <form onSubmit={this.onSubmit}>
          <div className="input-container-wrapper">
            <span className="input-label">Odaberi datum</span>
            <SingleDatePicker
              date={this.state.selectedDate}
              onDateChange={this.onDateChange}
              focused={this.state.focused}
              onFocusChange={this.onFocusChange}
              numberOfMonths={1}
              noBorder={true}
              isOutsideRange={day => !isInclusivelyAfterDay(day, moment())}
              hideKeyboardShortcutsPanel={true}
            />
          </div>
          <div className="input-container-wrapper">
            <span className="input-label">Generiraj</span>
            <InputContainer
              message={
                this.state.submitted && this.state.numberOfTimeslots.error
              }
              type="number"
              min="0"
              step="1"
              name="numberOfTimeslots"
              className={"inputFieldWhite "}
              value={this.state.numberOfTimeslots.value}
              onChange={this.handleUserInput}
            />
            <span className="input-label">termina.</span>
          </div>
          <div className="input-container-wrapper">
            <span className="input-label">Prvi termin počinje u</span>
            <InputObject
              message={this.state.submitted && this.state.startTime.error}
            >
              <TimePicker
                onFocusChange={this.handleTimeFocusChange}
                onTimeChange={this.handleTimeChange}
                time={`${this.state.hour}:${this.state.minute}`}
                timeMode="24"
              />
            </InputObject>
          </div>
          <div className="input-container-wrapper">
            <span className="input-label">Svaki termin traje</span>
            <InputContainer
              message={
                this.state.submitted && this.state.timeslotDuration.error
              }
              type="number"
              min="0"
              step="1"
              name="timeslotDuration"
              className={"inputFieldWhite "}
              value={this.state.timeslotDuration.value}
              onChange={this.handleUserInput}
            />
            <span className="input-label">sati.</span>
          </div>
          <div className="input-container-wrapper">
            <span className="input-label">Cijena</span>
            <InputContainer
              message={this.state.submitted && this.state.price.error}
              type="number"
              min="0"
              step="1"
              name="price"
              className={"inputFieldWhite "}
              value={this.state.price.value}
              onChange={this.handleUserInput}
            />
            <span className="input-label">HRK.</span>
          </div>
          <div className="input-container-wrapper">
            <BtnGreen
              label={"Dodaj"}
              submit={true}
              loading={this.state.loading}
            />
          </div>
        </form>
      </div>
    );
  }
}

export default connect()(TimeslotForm);
