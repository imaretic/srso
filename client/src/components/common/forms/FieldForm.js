import React from "react";
import BtnGreen from "../btns/BtnGreen";
import { connect } from "react-redux";
import { setNotification } from "../../../actions/notification";
import InputContainer from "../util/InputContainer";
import SelectContainer from "../util/SelectContainer";
import FormMessage from "../util/FormMessage";

class FieldForm extends React.Component {
  state = {
    loaded: false,
    sports: [],
    loading: false,
    submitted: false,
    formError: false,
    formMessage: "",
    name: {
      value: "",
      error: "Ime je obavezno!"
    },
    sportId: {
      value: 1,
      error: ""
    },
    numberOfPlayers: {
      value: "",
      error: "Broj igrača je obavezan!"
    },
    conditions: {
      value: "",
      error: "Uvjeti su obavezni!"
    }
  };

  componentDidMount = () => {
    fetch("/api/sports", {
      method: "GET"
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState(() => {
          return { loaded: true, sports: data };
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  validationFailed = () => {
    return (
      !!this.state.name.error ||
      !!this.state.sportId.error ||
      !!this.state.numberOfPlayers.error ||
      !!this.state.conditions.error
    );
  };

  handleUserInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => {
      return {
        touched: true,
        [name]: {
          value: value,
          error: this.props.validate(name, value)
        }
      };
    });
  };

  onSubmit = e => {
    e.preventDefault();
    this.setState(prevState => {
      return { loading: !prevState.loading, submitted: true };
    });

    if (this.validationFailed()) {
      this.setState(prevState => {
        return { loading: false };
      });
      return;
    }

    const token = localStorage.getItem("token");
    fetch(`/api/fields/create`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      },
      body: JSON.stringify({
        companyId: this.props.user.company.id,
        name: this.state.name.value,
        sportId: this.state.sportId.value,
        numberOfPlayers: this.state.numberOfPlayers.value,
        conditions: this.state.conditions.value
      })
    })
      .then(res => {
        // Validation failed
        if (res.status === 422) {
          const error = new Error();
          error.res = res.json();
          error.statusCode = 422;
          throw error;
        }
        // Some other problem occured
        if (res.status !== 200 && res.status !== 201) {
          const error = new Error(
            "Nesporazum u komunikaciji s bazom. Molimo pokušajte ponovo."
          );
          error.statusCode = res.status;
          throw error;
        }
        return res.json();
      })
      .then(resData => {
        this.setState(() => ({
          loading: false,
          submitted: false,
          name: {
            value: "",
            error: "Ime je obavezno!"
          },
          sportId: {
            value: 1,
            error: ""
          },
          numberOfPlayers: {
            value: "",
            error: "Broj igrača je obavezan!"
          },
          conditions: {
            value: "",
            error: "Uvjeti su obavezni!"
          }
        }));
        this.props.dispatch(
          setNotification({
            message: "Teren uspješno dodan!",
            error: false
          })
        );
        this.props.onSuccess(resData);
      })
      .catch(err => {
        if (err.statusCode === 422) {
          err.res.then(resData => {
            // Problem with fields
            resData.data.forEach(item => {
              this.setState(prevState => {
                return {
                  loading: false,
                  [item.param]: {
                    value: prevState[item.param].value,
                    error: item.msg
                  }
                };
              });
            });
          });
        } else {
          this.setState(() => {
            return {
              formError: true,
              formMessage: err.message,
              loading: false
            };
          });
        }
      });
  };

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <InputContainer
          message={this.state.submitted && this.state.name.error}
          type="text"
          name="name"
          value={this.state.name.value}
          className={
            "inputFieldWhite " +
            (this.state.submitted &&
              this.state.name.error &&
              `inputFieldWhite--error`)
          }
          placeholder="Naziv terena"
          onChange={this.handleUserInput}
        />

        <SelectContainer
          onChange={this.handleUserInput}
          name="sportId"
          className={
            "inputFieldWhite " +
            (this.state.submitted &&
              this.state.sportId.error &&
              `inputFieldWhite--error`)
          }
        >
          {this.state.sports.map(sport => {
            return (
              <option key={sport.id} value={sport.id}>
                {sport.name}
              </option>
            );
          })}
        </SelectContainer>

        <InputContainer
          message={this.state.submitted && this.state.numberOfPlayers.error}
          type="text"
          name="numberOfPlayers"
          value={this.state.numberOfPlayers.value}
          className={
            "inputFieldWhite " +
            (this.state.submitted &&
              this.state.numberOfPlayers.error &&
              `inputFieldWhite--error`)
          }
          placeholder="Broj igrača"
          onChange={this.handleUserInput}
        />
        <InputContainer
          message={this.state.submitted && this.state.conditions.error}
          type="text"
          name="conditions"
          value={this.state.conditions.value}
          className={
            "inputFieldWhite " +
            (this.state.submitted &&
              this.state.conditions.error &&
              `inputFieldWhite--error`)
          }
          placeholder="Uvjeti"
          onChange={this.handleUserInput}
        />
        {this.state.formMessage && (
          <FormMessage
            message={this.state.formMessage}
            error={this.state.formError}
          />
        )}
        <BtnGreen
          label={"Stvori teren"}
          submit={true}
          loading={this.state.loading}
        />
      </form>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(FieldForm);
