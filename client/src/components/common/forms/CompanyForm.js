import React from "react";
import { CountryDropdown } from "react-country-region-selector";
import BtnGreen from "../btns/BtnGreen";
import { connect } from "react-redux";
import { setNotification } from "../../../actions/notification";
import { Redirect } from "react-router";
import InputContainer from "../util/InputContainer";
import FormMessage from "../util/FormMessage";

class CompanyForm extends React.Component {
  state = {
    registered: false,
    updated: false,
    touched: false,
    loading: false,
    submitted: false,
    formError: false,
    formMessage: "",
    name: {
      value: "",
      error: "Ime je obavezno!"
    },
    email: {
      value: "",
      error: "Email je obavezan!"
    },
    phone: {
      value: "",
      error: "Kontakt broj je obavezan!"
    },
    street: {
      value: "",
      error: "Ulica je obavezna!"
    },
    streetNumber: {
      value: "",
      error: "Kućni broj je obavezan!"
    },
    city: {
      value: "",
      error: "Grad je obavezan!"
    },
    postCode: {
      value: "",
      error: "Poštanski broj je obavezan!"
    },
    country: {
      value: "Croatia",
      error: ""
    }
  };

  validationFailed = () => {
    return (
      !!this.state.name.error ||
      !!this.state.email.error ||
      !!this.state.phone.error ||
      !!this.state.street.error ||
      !!this.state.streetNumber.error ||
      !!this.state.city.error ||
      !!this.state.postCode.error ||
      !!this.state.country.error
    );
  };

  handleUserInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => {
      return {
        touched: true,
        [name]: {
          value: value,
          error: this.props.validate(name, value)
        }
      };
    });
  };

  onSubmit = e => {
    e.preventDefault();
    this.setState(prevState => {
      return { loading: !prevState.loading, submitted: true };
    });

    if (this.validationFailed()) {
      this.setState(prevState => {
        return { loading: false };
      });
      return;
    }

    const token = localStorage.getItem("token");
    fetch(`/api/companies/create`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      },
      body: JSON.stringify({
        name: this.state.name.value,
        email: this.state.email.value,
        phone: this.state.phone.value,
        street: this.state.street.value,
        streetNumber: this.state.streetNumber.value,
        city: this.state.city.value,
        postCode: this.state.postCode.value,
        country: this.state.country.value
      })
    })
      .then(res => {
        // Validation failed
        if (res.status === 422) {
          const error = new Error();
          error.res = res.json();
          error.statusCode = 422;
          throw error;
        }
        if (res.status === 403) {
          const error = new Error("Korisnik je već vlasnik tvrtke.");
          error.res = res.json();
          error.statusCode = 403;
          throw error;
        }
        // Some other problem occured
        if (res.status !== 200 && res.status !== 201) {
          const error = new Error(
            "Nesporazum u komunikaciji s bazom. Molimo pokušajte ponovo."
          );
          error.statusCode = res.status;
          throw error;
        }

        return res.json();
      })
      .then(resData => {
        this.setState(() => ({
          registered: true
        }));
        this.props.onSuccess();
      })
      .catch(err => {
        if (err.statusCode === 422) {
          err.res.then(resData => {
            // Problem with fields
            resData.data.forEach(item => {
              this.setState(prevState => {
                return {
                  loading: false,
                  [item.param]: {
                    value: prevState[item.param].value,
                    error: item.msg
                  }
                };
              });
            });
          });
        } else {
          this.props.dispatch(
            setNotification({
              message: "Došlo je do pogreške!",
              error: false
            })
          );
          this.setState(() => {
            return {
              formError: true,
              formMessage: err.message,
              loading: false
            };
          });
        }
      });
  };

  selectCountry = val => {
    this.setState({ country: { value: val, error: "" }, touched: true });
  };

  render() {
    if (this.state.registered) return <Redirect to="/mycompany" />;

    return (
      <form onSubmit={this.onSubmit}>
        <InputContainer
          message={this.state.submitted && this.state.name.error}
          type="text"
          name="name"
          value={this.state.name.value}
          className={
            "inputFieldWhite " +
            (this.state.submitted &&
              this.state.name.error &&
              `inputFieldWhite--error`)
          }
          placeholder="Naziv tvrtke"
          onChange={this.handleUserInput}
        />
        <InputContainer
          message={this.state.submitted && this.state.email.error}
          type="email"
          name="email"
          value={this.state.email.value}
          className={
            "inputFieldWhite " +
            (this.state.submitted &&
              this.state.email.error &&
              `inputFieldWhite--error`)
          }
          placeholder="E-mail"
          autoComplete="username"
          onChange={this.handleUserInput}
        />
        <InputContainer
          message={this.state.submitted && this.state.phone.error}
          type="phone"
          name="phone"
          value={this.state.phone.value}
          className={
            "inputFieldWhite " +
            (this.state.submitted &&
              this.state.phone.error &&
              `inputFieldWhite--error`)
          }
          placeholder="Broj mobitela"
          onChange={this.handleUserInput}
        />
        <InputContainer
          message={this.state.submitted && this.state.street.error}
          type="text"
          name="street"
          value={this.state.street.value}
          className={
            "inputFieldWhite " +
            (this.state.submitted &&
              this.state.street.error &&
              `inputFieldWhite--error`)
          }
          placeholder="Ulica"
          onChange={this.handleUserInput}
        />
        <InputContainer
          message={this.state.submitted && this.state.streetNumber.error}
          type="text"
          name="streetNumber"
          value={this.state.streetNumber.value}
          className={
            "inputFieldWhite " +
            (this.state.submitted &&
              this.state.streetNumber.error &&
              `inputFieldWhite--error`)
          }
          placeholder="Kućni broj"
          onChange={this.handleUserInput}
        />
        <InputContainer
          message={this.state.submitted && this.state.city.error}
          type="text"
          name="city"
          value={this.state.city.value}
          className={
            "inputFieldWhite " +
            (this.state.submitted &&
              this.state.city.error &&
              `inputFieldWhite--error`)
          }
          placeholder="Grad"
          onChange={this.handleUserInput}
        />
        <InputContainer
          message={this.state.submitted && this.state.postCode.error}
          type="text"
          name="postCode"
          value={this.state.postCode.value}
          className={
            "inputFieldWhite " +
            (this.state.submitted &&
              this.state.postCode.error &&
              `inputFieldWhite--error`)
          }
          placeholder="Poštanski broj"
          onChange={this.handleUserInput}
        />

        <div className="input-container">
          <CountryDropdown
            value={this.state.country.value}
            className="inputFieldWhite"
            onChange={val => this.selectCountry(val)}
          />
        </div>

        {this.state.formMessage && (
          <FormMessage
            message={this.state.formMessage}
            error={this.state.formError}
          />
        )}
        <BtnGreen
          label={"Registriraj tvrtku"}
          submit={true}
          loading={this.state.loading}
        />
      </form>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(CompanyForm);
