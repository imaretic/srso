import React from "react";
import { connect } from "react-redux";
import { setUser } from "../../../actions/user";
import { setNotification } from "../../../actions/notification";
import BtnGreen from "../btns/BtnGreen";
import InputContainer from "../util/InputContainer";
import FormMessage from "../util/FormMessage";

class LoginForm extends React.Component {
  state = {
    loading: false,
    submitted: false,
    formError: false,
    formMessage: "",
    email: {
      value: "",
      error: "Email je obavezan!"
    },
    password: {
      value: "",
      error: "Lozinka je obavezna!"
    }
  };

  validationFailed = () => {
    return !!this.state.email.error || !!this.state.password.error;
  };

  handleUserInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => {
      return {
        [name]: {
          value: value,
          error: this.props.validate(name, value)
        }
      };
    });
  };

  onSubmit = e => {
    e.preventDefault();
    this.setState(prevState => {
      return { loading: !prevState.loading, submitted: true };
    });

    if (this.validationFailed()) {
      this.setState(prevState => {
        return { loading: false };
      });
      return;
    }

    fetch("/api/auth/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: this.state.email.value,
        password: this.state.password.value
      })
    })
      .then(res => {
        // Validation failed
        if (res.status === 422) {
          const error = new Error();
          error.res = res.json();
          error.statusCode = 422;
          throw error;
        }
        // Some other problem occured
        if (res.status !== 200 && res.status !== 201) {
          const error = new Error(
            "Nesporazum u komunikaciji s bazom. Molimo pokušajte ponovo."
          );
          error.statusCode = res.status;
          throw error;
        }

        return res.json();
      })
      .then(resData => {
        this.props.dispatch(setUser(resData.user));
        this.props.dispatch(
          setNotification({
            message: "Uspješna prijava!",
            error: false
          })
        );
        localStorage.setItem("token", resData.token);
        localStorage.setItem("userId", resData.user.id);
      })
      .catch(err => {
        if (err.statusCode === 422 && err.res !== undefined) {
          err.res.then(resData => {
            if (resData.data !== undefined) {
              // Problem with fields
              resData.data.forEach(item => {
                this.setState(prevState => {
                  return {
                    loading: false,
                    [item.param]: {
                      value: prevState[item.param].value,
                      error: item.msg
                    }
                  };
                });
              });
            } else {
              // Incorrect match
              this.setState(() => {
                return {
                  formError: true,
                  formMessage: "Email i(li) lozinka nisu valjani.",
                  loading: false
                };
              });
            }
          });
        } else {
          this.setState(() => {
            return {
              formError: true,
              formMessage: err.message,
              loading: false
            };
          });
        }
      });
  };

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <InputContainer
          message={this.state.submitted && this.state.email.error}
          type="email"
          name="email"
          value={this.state.email.value}
          className={
            "inputFieldWhite " +
            ((this.state.submitted && this.state.email.error) ||
            this.state.formError
              ? "inputFieldWhite--error"
              : "")
          }
          placeholder="E-mail"
          autoComplete="username"
          onChange={this.handleUserInput}
        />

        <InputContainer
          message={this.state.submitted && this.state.password.error}
          type="password"
          name="password"
          value={this.state.password.value}
          className={
            "inputFieldWhite " +
            ((this.state.submitted && this.state.password.error) ||
            this.state.formError
              ? "inputFieldWhite--error"
              : "")
          }
          placeholder="Lozinka"
          autoComplete="current-password"
          onChange={this.handleUserInput}
        />

        {this.state.formMessage && (
          <FormMessage
            message={this.state.formMessage}
            error={this.state.formError}
          />
        )}
        <BtnGreen label="Prijava" submit={true} loading={this.state.loading} />
      </form>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(LoginForm);
