import React from "react";
import BtnGreen from "../btns/BtnGreen";
import { connect } from "react-redux";
import InputContainer from "../util/InputContainer";
import InputObject from "../util/InputObject";
import FormMessage from "../util/FormMessage";

class FieldForm extends React.Component {
  state = {
    loading: false,
    submitted: false,
    formError: false,
    formMessage: "",
    name: {
      value: "",
      error: "Ime je obavezno!"
    },
    image: {
      value: "",
      name: "Slika nije odabrana.",
      error: "Slika je obavezna!"
    }
  };

  validationFailed = () => {
    return !!this.state.name.error;
  };

  handleUserInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => {
      return {
        touched: true,
        [name]: {
          value: value,
          error: this.props.validate(name, value)
        }
      };
    });
  };

  onSubmit = e => {
    e.preventDefault();
    this.setState(prevState => {
      return { loading: !prevState.loading, submitted: true };
    });

    if (this.validationFailed()) {
      this.setState(prevState => {
        return { loading: false };
      });
      return;
    }

    const formData = new FormData();
    formData.append("name", this.state.name.value);
    formData.append("image", this.state.image.value);

    const token = localStorage.getItem("token");
    fetch(`/api/sports/create`, {
      method: "PUT",
      headers: {
        Authorization: "Bearer " + token
      },
      body: formData
    })
      .then(res => {
        // Validation failed
        if (res.status === 422) {
          const error = new Error();
          error.res = res.json();
          error.statusCode = 422;
          throw error;
        }

        // Some other problem occured
        if (res.status !== 200 && res.status !== 201) {
          const error = new Error(
            "Nesporazum u komunikaciji s bazom. Molimo pokušajte ponovo."
          );
          error.statusCode = res.status;
          throw error;
        }

        return res.json();
      })
      .then(resData => {
        this.setState(() => {
          return {
            loading: false,
            name: {
              value: "",
              error: "Ime je obavezno!"
            },
            image: {
              value: null,
              name: "Slika nije odabrana.",
              error: "Slika je obavezna!"
            },
            submitted: false
          };
        });
        this.props.onSuccess(resData.sport);
      })
      .catch(err => {
        if (err.statusCode === 422) {
          err.res.then(resData => {
            // Problem with fields
            resData.data.forEach(item => {
              this.setState(prevState => {
                return {
                  loading: false,
                  [item.param]: {
                    value: prevState[item.param].value,
                    error: item.msg
                  }
                };
              });
            });
          });
        } else {
          this.setState(() => {
            return {
              formError: true,
              formMessage: err.message,
              loading: false
            };
          });
        }
      });
  };

  handleFileSelected = e => {
    const file = e.target.files[0];

    this.setState({
      image: {
        value: file ? file : null,
        name: file ? file.name : "Slika nije odabrana.",
        error: file ? "" : "Slika je obavezna!"
      }
    });
  };

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <InputContainer
          message={this.state.submitted && this.state.name.error}
          type="text"
          name="name"
          value={this.state.name.value}
          className={
            "inputFieldWhite " +
            (this.state.submitted &&
              this.state.name.error &&
              `inputFieldWhite--error`)
          }
          placeholder="Naziv sporta"
          onChange={this.handleUserInput}
        />
        <InputObject message={this.state.submitted && this.state.image.error}>
          <input
            type="file"
            name="image"
            id="image"
            className="inputfile"
            onChange={this.handleFileSelected}
          />
          <label htmlFor="image" className="btn btn--green btn--green-s">
            Odaberi sliku
          </label>
          <span className="fileName">{this.state.image.name}</span>
        </InputObject>
        {this.state.formMessage && (
          <FormMessage
            message={this.state.formMessage}
            error={this.state.formError}
          />
        )}
        <BtnGreen
          label={"Dodaj sport"}
          submit={true}
          loading={this.state.loading}
        />
      </form>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(FieldForm);
