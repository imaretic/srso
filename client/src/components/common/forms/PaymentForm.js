import React from "react";
import { connect } from "react-redux";
import { setNotification } from "../../../actions/notification";
import BtnGreen from "../btns/BtnGreen";
import { CardElement, injectStripe } from "react-stripe-elements";

class PaymentForm extends React.Component {
  state = {
    loading: false
  };

  handlePay = e => {
    this.setState(() => ({ loading: true }));

    this.props.stripe
      .createToken({ name: "Name" })
      .then(token => {
        const tokenError = !!token.error;
        if (tokenError) {
          this.setState(() => ({ loading: false }));
          const error = new Error();
          error.statusCode = 422;
          throw error;
        }

        return fetch(`/api/participations/pay/${this.props.participationId}`, {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({
            token: token.token.id
          })
        });
      })
      .then(res => {
        this.setState(() => ({ loading: false }));
        if (res.ok) {
          this.props.onSuccess(this.props.participationId);
        } else {
          throw new Error("Došlo je do pogreške pri plaćanju!");
        }
        return res.json();
      })
      .catch(err => {
        this.props.dispatch(
          setNotification({
            message: "Došlo je do pogreške pri plaćanju!",
            error: true
          })
        );
        console.log(err);
      });
  };

  render() {
    return (
      <div className="stripe-checkout">
        <h4>PLAĆANJE KARTICOM ({this.props.price.toFixed(2)}HRK)</h4>
        <div className="stripe-card-wrapper">
          <CardElement />
        </div>
        <BtnGreen
          onClick={this.handlePay}
          loading={this.state.loading}
          label="PLATI"
        />
      </div>
    );
  }
}

export default connect()(injectStripe(PaymentForm));
