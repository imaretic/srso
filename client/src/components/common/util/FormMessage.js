import React from "react";

class FormMessage extends React.Component {
  render() {
    return (
      <div
        className={
          "form-message " + (this.props.error ? "form-message--error" : "")
        }
      >
        {this.props.message}
      </div>
    );
  }
}

export default FormMessage;
