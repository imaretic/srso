import React from "react";
import success from "../../../img/success_notif.svg";
import error from "../../../img/error_notif.svg";

class Notification extends React.Component {
  render() {
    return (
      <div
        className={
          "notification " +
          (!this.props.error ? "notification--success" : "notification--error")
        }
      >
        {!this.props.error ? (
          <img src={success} alt="Success" />
        ) : (
          <img src={error} alt="Error" />
        )}

        <span>{this.props.message}</span>
      </div>
    );
  }
}

export default Notification;
