import React from "react";

class SectionTitle extends React.Component {
  render() {
    return (
      <div className="section-title">
        <h2>{this.props.label}</h2>
        <div className="section-title__line" />
      </div>
    );
  }
}

export default SectionTitle;
