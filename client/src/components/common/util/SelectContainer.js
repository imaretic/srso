import React from "react";
import { CSSTransition } from "react-transition-group";
import InputMessage from "./InputMessage";

class SelectContainer extends React.Component {
  render() {
    const { message, label, edit, ...others } = this.props;
    return (
      <div className="input-container">
        <select {...others}>{this.props.children}</select>

        <CSSTransition
          in={!!message}
          timeout={200}
          classNames="input-message"
          unmountOnExit
        >
          <InputMessage message={message} />
        </CSSTransition>
      </div>
    );
  }
}

export default SelectContainer;
