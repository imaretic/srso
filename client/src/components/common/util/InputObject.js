import React from "react";
import { CSSTransition } from "react-transition-group";
import InputMessage from "./InputMessage";

const InputObject = props => {
  const { message } = props;
  return (
    <div className="input-container">
      {props.children}
      <CSSTransition
        in={!!message}
        timeout={200}
        classNames="input-message"
        unmountOnExit
      >
        <InputMessage message={message} />
      </CSSTransition>
    </div>
  );
};

export default InputObject;
