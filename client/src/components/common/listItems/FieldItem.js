import React from "react";
import { connect } from "react-redux";
import { setNotification } from "../../../actions/notification";
import BtnRedSmall from "../btns/BtnRedSmall";
import BtnGreenSmall from "../btns/BtnGreenSmall";

class FieldItem extends React.Component {
  handleDelete = () => {
    fetch(`/api/fields/${this.props.id}`, {
      method: "DELETE"
    })
      .then(res => {
        if (res.status !== 200) {
          const error = new Error("Pogreška pri brisanju terena.");
          error.statusCode = res.status;
          throw error;
        }
        return res.json();
      })
      .then(resData => {
        this.props.onDeleteSuccess(this.props.id);
      })
      .catch(err => {
        this.props.dispatch(
          setNotification({
            visible: true,
            message: "Došlo je do pogreške!",
            error: true
          })
        );
        this.setState(() => ({
          loaded: true
        }));
      });
  };

  render() {
    return (
      <li className="h-list--small__item">
        <div className="h-list--small__item__header">
          <h5>
            {this.props.name}
            <span>{this.props.note}</span>
          </h5>
          <div className="h-list--small__item__header__btns">
            <BtnGreenSmall
              label="Termini"
              to={`/mycompany/field/${this.props.id}`}
            />
            <BtnRedSmall label="Izbriši" onClick={this.handleDelete} />
          </div>
        </div>
      </li>
    );
  }
}

export default connect()(FieldItem);
