import React from "react";
import { connect } from "react-redux";
import { setNotification } from "../../../actions/notification";
import clock from "../../../img/clock.svg";
import AnimateHeight from "react-animate-height";
import BtnGreenSmall from "../btns/BtnGreenSmall";
import { Elements, StripeProvider } from "react-stripe-elements";
import PaymentForm from "../forms/PaymentForm";

class TimeslotItemExp extends React.Component {
  state = {
    visible: false,
    btnLabel: "Prikaži više"
  };

  toggle = () => {
    this.setState(prevState => {
      return {
        btnLabel: prevState.visible ? "Prikaži više" : "Sakrij",
        visible: !prevState.visible
      };
    });
  };

  handlePaymentSuccess = paymentId => {
    this.props.dispatch(
      setNotification({
        visible: true,
        message: "Transakcija uspješna!",
        error: false
      })
    );
    this.props.onPaymentSuccess(paymentId);
  };

  render() {
    return (
      <li className="hl-timeslot__item">
        <div className="hl-timeslot__item__header">
          <img src={clock} alt="Clock" />
          <h5>
            <span className="startTime">{this.props.startTime}</span>
            <span className="endTime">do {this.props.endTime}</span>
            <span className="date">{this.props.date}</span>
            <span className="note">
              {parseFloat(this.props.price).toFixed(2)}
            </span>
            <span
              className={
                `note ` + (this.props.paid ? "note--paid" : "note--notpaid")
              }
            >
              {this.props.paid ? "PLAĆENO" : "NIJE PLAĆENO"}
            </span>
          </h5>
          <div className="hl-timeslot__item__header__btns">
            <BtnGreenSmall label={this.state.btnLabel} onClick={this.toggle} />
          </div>
        </div>

        <AnimateHeight
          duration={300}
          height={this.state.visible ? "auto" : "0"}
        >
          <div className="hl-timeslot__item__content">
            <div className="hl-timeslot__item__content__meta">
              <table>
                <tbody>
                  <tr>
                    <td>Rezervaciju napravio/la:</td>
                    <td>
                      <strong>{this.props.author}</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>Teren:</td>
                    <td>
                      <strong>{this.props.field}</strong> ({this.props.fieldId})
                    </td>
                  </tr>
                  <tr>
                    <td>Tvrtka:</td>
                    <td>
                      <strong>{this.props.company}</strong> (
                      {this.props.companyId})
                    </td>
                  </tr>
                  <tr>
                    <td>Korisnici:</td>
                    <td>
                      {this.props.users.map(user => {
                        return (
                          <div key={user.id}>{`${user.firstName} ${
                            user.lastName
                          } (${user.id})`}</div>
                        );
                      })}
                    </td>
                  </tr>
                  <tr>
                    <td>ID sudjelovanja:</td>
                    <td>{this.props.participationId}</td>
                  </tr>
                  <tr>
                    <td>ID rezervacije:</td>
                    <td>{this.props.reservationId}</td>
                  </tr>
                  <tr>
                    <td>Ukupna cijena:</td>
                    <td>{parseFloat(this.props.price).toFixed(2)}HRK</td>
                  </tr>
                  <tr>
                    <td>Cijena po osobi:</td>
                    <td>
                      <strong>
                        {(
                          parseFloat(this.props.price) /
                          parseFloat(this.props.users.length)
                        ).toFixed(2)}
                        HRK
                      </strong>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="hl-timeslot__item__content__btns">
              {!this.props.paid && (
                <StripeProvider apiKey="pk_test_lGm2FnKxYtiGcggKFzUHveWu">
                  <div>
                    <Elements>
                      <PaymentForm
                        participationId={this.props.id}
                        onSuccess={this.handlePaymentSuccess}
                        price={
                          parseFloat(this.props.price) /
                          parseFloat(this.props.users.length)
                        }
                      />
                    </Elements>
                  </div>
                </StripeProvider>
              )}
            </div>
          </div>
        </AnimateHeight>
      </li>
    );
  }
}

export default connect()(TimeslotItemExp);
