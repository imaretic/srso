import React from "react";
import { connect } from "react-redux";
import AnimateHeight from "react-animate-height";
import BtnGreenSmall from "../btns/BtnGreenSmall";
import BtnRedSmall from "../btns/BtnRedSmall";
import avatar from "../../../img/avatar.svg";

class AdminUserItem extends React.Component {
  state = {
    visible: false,
    btnLabel: "Prikaži više",
    deleted: false,
    isAdmin: false,
    deleteLoading: false,
    adminLoading: false
  };

  toggle = () => {
    this.setState(prevState => {
      return {
        btnLabel: prevState.visible ? "Prikaži više" : "Sakrij",
        visible: !prevState.visible
      };
    });
  };

  toggleAdmin = () => {
    this.setState(() => {
      return { adminLoading: true };
    });
    const userId = this.props.id;
    const token = localStorage.getItem("token");
    fetch(`/api/users/toggleadmin`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ userId: userId })
    })
      .then(res => {
        this.setState(() => {
          return { adminLoading: false };
        });
        // Validation failed
        if (res.status === 422) {
          const error = new Error();
          error.res = res.json();
          error.statusCode = 422;
          throw error;
        }
        // Some other problem occured
        if (res.status !== 200 && res.status !== 201) {
          const error = new Error(
            "Nesporazum u komunikaciji s bazom. Molimo pokušajte ponovo."
          );
          error.statusCode = res.status;
          throw error;
        }
        if (res.status === 200) {
          // Successfully toggled admin role
          this.props.onToggleAdminSuccess(this.props.id);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  deleteUser = () => {
    this.setState(() => {
      return { deleteLoading: true };
    });
    const userId = this.props.id;
    const token = localStorage.getItem("token");
    fetch(`/api/users/${userId}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        this.setState(() => {
          return { deleteLoading: false };
        });
        // Validation failed
        if (res.status === 422) {
          const error = new Error();
          error.res = res.json();
          error.statusCode = 422;
          throw error;
        }
        // Some other problem occured
        if (res.status !== 200 && res.status !== 201) {
          const error = new Error(
            "Nesporazum u komunikaciji s bazom. Molimo pokušajte ponovo."
          );
          error.statusCode = res.status;
          throw error;
        }
        if (res.status === 200) {
          this.props.onDeleteUserSuccess(this.props.id);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    return (
      <li className="h-list-user-item">
        <div className="h-list-user-item-header">
          <img alt="Avatar" src={avatar} />
          <h5>{`${this.props.firstName} ${this.props.lastName}`}</h5>
          <div className="h-list-user-item-header__btns">
            <BtnGreenSmall label={this.state.btnLabel} onClick={this.toggle} />
          </div>
        </div>
        <AnimateHeight
          duration={300}
          height={this.state.visible ? "auto" : "0"}
        >
          <div className="h-list-user-item-content">
            <div className="h-list-user-item-content__meta">
              <table>
                <tbody>
                  <tr>
                    <td>Ime</td>
                    <td>{this.props.firstName}</td>
                  </tr>
                  <tr>
                    <td>Prezime</td>
                    <td>{this.props.lastName}</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>{this.props.email}</td>
                  </tr>
                  <tr>
                    <td>Telefon</td>
                    <td>{this.props.phone}</td>
                  </tr>
                  <tr>
                    <td>Država</td>
                    <td>{this.props.country}</td>
                  </tr>
                </tbody>
              </table>
            </div>
            {this.props.id !== this.props.user.id && (
              <div className="h-list-user-item-content__btns">
                <BtnRedSmall
                  label="Izbriši račun"
                  loading={this.state.deleteLoading}
                  onClick={e => {
                    this.deleteUser();
                  }}
                />
                <BtnGreenSmall
                  label={
                    this.props.isAdmin ? "Ukloni admina" : "Postavi admina"
                  }
                  loading={this.state.adminLoading}
                  onClick={e => {
                    this.toggleAdmin();
                  }}
                />
              </div>
            )}
          </div>
        </AnimateHeight>
      </li>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(AdminUserItem);
