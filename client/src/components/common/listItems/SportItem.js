import React from "react";
import { connect } from "react-redux";
import { setNotification } from "../../../actions/notification";
import BtnRedSmall from "../btns/BtnRedSmall";

class SportItem extends React.Component {
  state = {
    loading: false
  };

  handleDelete = () => {
    this.setState(() => {
      return { loading: true };
    });

    const token = localStorage.getItem("token");
    fetch(`/api/sports/delete/${this.props.id}`, {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + token
      }
    })
      .then(res => {
        if (res.status !== 200) {
          const error = new Error("Pogreška pri brisanju sporta.");
          error.statusCode = res.status;
          throw error;
        }
        if (res.status === 200) {
          this.setState(() => {
            return { loading: false };
          });
          this.props.onDeleteSuccess(this.props.id);
        }
      })
      .catch(err => {
        this.props.dispatch(
          setNotification({
            message: err.message,
            error: true
          })
        );
        this.setState(() => ({
          loading: false
        }));
      });
  };

  render() {
    return (
      <li className="hl-sport__item">
        <div className="hl-sport__item__header">
          <h5>
            {this.props.name}
            {this.props.note && <span>{this.props.note}</span>}
          </h5>
          <div className="hl-sport__item__header__btns">
            <BtnRedSmall
              label="Izbriši"
              onClick={this.handleDelete}
              loading={this.state.loading}
            />
          </div>
        </div>
      </li>
    );
  }
}

export default connect()(SportItem);
