import React from "react";
import { connect } from "react-redux";
import { setNotification } from "../../../actions/notification";
import BtnRedSmall from "../btns/BtnRedSmall";
import clock from "../../../img/clock.svg";
import BtnGreenSmall from "../btns/BtnGreenSmall";

class TimeslotItem extends React.Component {
  state = {
    asPollOption: false,
    loading: false
  };

  handleDelete = () => {
    this.setState(() => {
      return { loading: true };
    });

    const token = localStorage.getItem("token");
    fetch(`/api/timeslots/${this.props.id}`, {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + token
      }
    })
      .then(res => {
        if (res.status !== 200) {
          const error = new Error("Pogreška pri brisanju terena.");
          error.statusCode = res.status;
          throw error;
        }
        if (res.status === 200) {
          this.setState(() => {
            return { loading: false };
          });
          this.props.onDeleteSuccess(this.props.id);
        }
      })
      .catch(err => {
        this.props.dispatch(
          setNotification({
            message: err.message,
            error: true
          })
        );
        this.setState(() => ({
          loading: false
        }));
      });
  };

  componentDidMount = () => {
    let pollOptions = localStorage.getItem("pollOptions");
    if (!pollOptions) return;

    pollOptions = JSON.parse(pollOptions);
    const found = pollOptions.find(timeslotId => {
      return timeslotId === this.props.id;
    });

    if (found) {
      this.setState(() => {
        return { asPollOption: true };
      });
    }
  };

  handleAddPollOption = () => {
    let pollOptions = localStorage.getItem("pollOptions");

    if (!pollOptions) {
      pollOptions = [this.props.id];
    } else {
      pollOptions = JSON.parse(pollOptions);
      pollOptions.push(this.props.id);
    }

    localStorage.setItem("pollOptions", JSON.stringify(pollOptions));

    this.setState(() => {
      return { asPollOption: true };
    });
  };

  render() {
    return (
      <li className="hl-timeslot__item">
        <div className="hl-timeslot__item__header">
          <img src={clock} alt="Clock" />
          <h5>
            <span className="startTime">{this.props.startTime}</span>
            <span className="endTime">do {this.props.endTime}</span>
            <span className="note">{this.props.price}</span>
            {this.props.reserved && (
              <span className="note note--reserved">REZERVIRANO</span>
            )}
          </h5>
          <div className="hl-timeslot__item__header__btns">
            {!this.props.reserved && !this.state.asPollOption && (
              <BtnGreenSmall
                label="Dodaj u novu anketu"
                onClick={this.handleAddPollOption}
              />
            )}
            {this.props.to && !this.props.reserved && (
              <BtnGreenSmall label={this.props.toLabel} to={this.props.to} />
            )}
            {this.props.onDeleteSuccess && !this.props.reserved && (
              <BtnRedSmall
                label="Izbriši"
                onClick={this.handleDelete}
                loading={this.state.loading}
              />
            )}
          </div>
        </div>
      </li>
    );
  }
}

export default connect()(TimeslotItem);
