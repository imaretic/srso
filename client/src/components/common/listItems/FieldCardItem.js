import React from "react";
import BtnGreenSmallWide from "../btns/BtnGreenSmallWide";
import people from "../../../img/people.svg";

class FieldCardItem extends React.Component {
  render() {
    return (
      <li>
        <img
          className="field-card-image"
          src={"http://localhost:5000/" + this.props.imageUrl}
          alt={this.props.sport}
        />
        <div className="field-card-content">
          <div className="field-card-sport">
            <span>{this.props.sport}</span>
          </div>
          <h3>{this.props.name}</h3>
          <div className="field-card-people">
            <img src={people} alt="People" />
            <p>{this.props.numberOfPlayers} ljudi</p>
          </div>
          <div className="field-card-reserve">
            <BtnGreenSmallWide
              label="Rezerviraj"
              to={`/field/${this.props.id}`}
            />
          </div>
        </div>
      </li>
    );
  }
}

export default FieldCardItem;
