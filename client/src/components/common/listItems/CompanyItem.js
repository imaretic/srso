import React from "react";
import hlistArrow from "../../../img/hlist-arrow.svg";
import hlistDefault from "../../../img/object_default.jpg";
import { NavLink } from "react-router-dom";

class HListItem extends React.Component {
  render() {
    const divStyle = {
      backgroundImage: "url(" + hlistArrow + ")"
    };

    return (
      <li>
        <NavLink to={`/company/${this.props.id}/${encodeURI(this.props.name)}`}>
          <img className="hl-company__image" src={hlistDefault} alt="Field" />
          <div className="hl-company__content">
            <h3>{this.props.name}</h3>
            <span>{this.props.address}</span>
          </div>
          <div className="hl-company__arrow">
            <div style={divStyle} />
          </div>
        </NavLink>
      </li>
    );
  }
}

export default HListItem;
