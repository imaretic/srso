import React from "react";
import BtnRedSmall from "../btns/BtnRedSmall";
import clock from "../../../img/clock.svg";
import BtnGreenSmall from "../btns/BtnGreenSmall";

class PollOptionItem extends React.Component {
  state = {
    asPollOption: false,
    loading: false
  };

  componentDidMount = () => {
    let pollOptions = localStorage.getItem("pollOptions");
    if (!pollOptions) return;

    pollOptions = JSON.parse(pollOptions);
    const found = pollOptions.find(timeslotId => {
      return timeslotId === this.props.id;
    });

    if (found) {
      this.setState(() => {
        return { asPollOption: true };
      });
    }
  };

  handleVoteOption = () => {
    this.setState(() => {
      return { loading: true };
    });
    const token = localStorage.getItem("token");
    console.log("GLASAM ZA: ", this.props.id);
    fetch(`/api/votes/vote/${this.props.id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      }
    })
      .then(res => {
        if (res.ok) {
          this.setState(() => {
            return { loading: false };
          });
          this.props.onVoteSuccess();
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleDeleteOption = () => {
    let pollOptions = localStorage.getItem("pollOptions");

    if (!pollOptions) return;

    pollOptions = JSON.parse(pollOptions);
    pollOptions = pollOptions.filter(pollOption => {
      return pollOption !== this.props.id;
    });

    localStorage.setItem("pollOptions", JSON.stringify(pollOptions));

    this.props.onDeleteSuccess();
  };

  render() {
    const style = {
      width: this.props.percentage + "%"
    };

    return (
      <li className="hl-polloption__item">
        {this.props.onVoteSuccess && (
          <React.Fragment>
            <div className="percentage">{this.props.percentage}%</div>
            <div className="percentage-line" style={style} />
          </React.Fragment>
        )}
        <div className="hl-polloption__item__header">
          <img src={clock} alt="Clock" />
          <h5>
            <span className="startTime">{this.props.startTime}</span>
            <span className="endTime">do {this.props.endTime}</span>
            <span className="date">{this.props.date}</span>
            <span className="note">{this.props.price}</span>
            <span className="note">{this.props.sportfield}</span>
            <span className="note">{this.props.company}</span>
          </h5>
          <div className="hl-polloption__item__header__btns">
            {this.props.onVoteSuccess && this.props.votable && (
              <BtnGreenSmall
                label="Glasaj"
                onClick={this.handleVoteOption}
                loading={this.state.loading}
              />
            )}
            {this.props.onDeleteSuccess && (
              <BtnRedSmall label="Izbriši" onClick={this.handleDeleteOption} />
            )}
          </div>
        </div>
      </li>
    );
  }
}

export default PollOptionItem;
