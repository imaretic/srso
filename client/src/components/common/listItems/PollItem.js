import React from "react";
import { Link } from "react-router-dom";

class PollItem extends React.Component {
  render() {
    return (
      <li className="hl-poll__item">
        <Link to={`/poll/${this.props.id}`}>
          <div className="hl-poll__item__header">
            <h5>{this.props.name}</h5>
          </div>
        </Link>
      </li>
    );
  }
}

export default PollItem;
