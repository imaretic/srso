import React from "react";
import { Helmet } from "react-helmet";
import SportForm from "../common/forms/SportForm";
import SportValidator from "../common/validators/SportValidator";
import SportItem from "../common/listItems/SportItem";
import PageLoader from "../common/loader/PageLoader";

class SportsPage extends React.Component {
  state = {
    loaded: false,
    data: []
  };

  componentDidMount = () => {
    this.reloadSports();
  };

  reloadSports = () => {
    fetch("/api/sports", {
      method: "GET"
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState(() => {
          return { loaded: true, data: data };
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleSuccess = newSport => {
    this.setState(prevState => {
      return { data: [...prevState.data, newSport] };
    });
  };

  handleDeleteSuccess = () => {
    this.reloadSports();
  };

  render() {
    if (!this.state.loaded) return <PageLoader />;

    return (
      <section id="sports-page" className="page">
        <Helmet>
          <title>Sportovi | Rezerviraj termin online</title>
        </Helmet>
        <div className="container">
          <h1 className="page-title">Sportovi</h1>
          <ul className="hl-sport">
            {this.state.data.map(sport => {
              return (
                <SportItem
                  key={sport.id}
                  id={sport.id}
                  name={sport.name}
                  onDeleteSuccess={this.handleDeleteSuccess}
                />
              );
            })}
          </ul>
          <div className="page-form page-form--center">
            <SportValidator>
              <SportForm onSuccess={this.handleSuccess} />
            </SportValidator>
          </div>
        </div>
      </section>
    );
  }
}

export default SportsPage;
