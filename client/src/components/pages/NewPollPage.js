import React from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import PageLoader from "../common/loader/PageLoader";
import PollOptionItem from "../common/listItems/PollOptionItem";
import moment from "moment";
import BtnGreen from "../common/btns/BtnGreen";

class NewPoll extends React.Component {
  state = {
    data: [],
    loaded: false,
    pollId: undefined
  };

  componentDidMount = () => {
    this.reloadPollOptions();
  };

  handleDeleteSuccess = () => {
    this.reloadPollOptions();
  };

  reloadPollOptions = () => {
    let pollOptions = localStorage.getItem("pollOptions");
    if (!pollOptions) {
      this.setState(() => {
        return { loaded: true };
      });
      return;
    }

    fetch(`/api/timeslots/bylist/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        timeslots: JSON.parse(pollOptions)
      })
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState(() => {
          return { loaded: true, data: data };
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  onSubmit = e => {
    e.preventDefault();

    const token = localStorage.getItem("token");
    const timeslots = this.state.data.map(timeslot => {
      return timeslot.id;
    });

    fetch(`/api/polls/create/`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      },
      body: JSON.stringify({
        timeslots: timeslots
      })
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        localStorage.removeItem("pollOptions");
        this.setState(() => {
          return { pollId: data.poll.id };
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    if (!this.state.loaded) return <PageLoader />;
    if (this.state.pollId)
      return <Redirect to={`/poll/${this.state.pollId}`} />;

    return (
      <section id="poll-page" className="page">
        <Helmet>
          <title>Nova anketa | Rezerviraj termin online</title>
        </Helmet>
        <div className="container">
          <h1 className="page-title page-title--left">Nova anketa</h1>
          {this.state.data.length === 0 ? (
            <p className="page-message">Nema dodanih termina.</p>
          ) : (
            <form onSubmit={this.onSubmit} className="page-btn">
              <BtnGreen label="Stvori anketu" submit={true} />
            </form>
          )}
          <ul className="hl-polloption">
            {this.state.data.map(timeslot => {
              return (
                <PollOptionItem
                  key={timeslot.id}
                  id={timeslot.id}
                  votable={false}
                  startTime={moment(timeslot.startTime).format("HH:mm")}
                  endTime={moment(timeslot.endTime).format("HH:mm")}
                  price={`${timeslot.price}HRK`}
                  sportfield={timeslot.sportfield.name}
                  company={timeslot.sportfield.company.name}
                  onDeleteSuccess={this.handleDeleteSuccess}
                />
              );
            })}
          </ul>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(NewPoll);
