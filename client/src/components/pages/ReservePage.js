import React from "react";
import { Helmet } from "react-helmet";
import { Redirect } from "react-router";
import { connect } from "react-redux";
import { setNotification } from "../../actions/notification";
import PageLoader from "../common/loader/PageLoader";
import TimeslotItem from "../common/listItems/TimeslotItem";
import SectionTitle from "../common/util/SectionTitle";
import BtnGreen from "../common/btns/BtnGreen";
import moment from "moment";
import FormMessage from "../common/util/FormMessage";
import InputContainer from "../common/util/InputContainer";
import BtnAdd from "../common/btns/BtnAdd";
import BtnDelete from "../common/btns/BtnDelete";

class ReservePage extends React.Component {
  state = {
    loaded: false,
    reserved: false,
    loading: false,
    formError: true,
    formMessage: "",
    timeslot: {},
    users: []
  };

  componentDidMount = () => {
    fetch(`/api/timeslots/${this.props.match.params.timeslotId}?extra=true`, {
      method: "GET"
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            loaded: true,
            timeslot: data
          };
        });
      })
      .catch(err => {
        this.props.dispatch(
          setNotification({
            visible: true,
            message: err,
            error: true
          })
        );
        console.log(err);
      });
  };

  validationFailed = () => {
    let notValid = false;
    this.state.users.forEach(user => {
      if (!user.id) notValid = true;
    });

    return notValid;
  };

  onSubmit = e => {
    e.preventDefault();
    this.setState(prevState => {
      return { loading: !prevState.loading };
    });

    if (this.validationFailed()) {
      this.setState(prevState => {
        return { loading: false };
      });
      return;
    }

    const token = localStorage.getItem("token");
    const users = this.state.users.map(user => {
      return user.id;
    });

    fetch(`/api/reservations/create`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      },
      body: JSON.stringify({
        timeslotId: this.state.timeslot.id,
        users: users
      })
    })
      .then(res => {
        // Some other problem occured
        if (res.status === 403) {
          const error = new Error("Termin je već rezerviran.");
          error.statusCode = res.status;
          throw error;
        }
        if (res.status !== 200 && res.status !== 201) {
          const error = new Error("Došlo je do pogreške.");
          error.statusCode = res.status;
          throw error;
        }
        return res.json();
      })
      .then(resData => {
        this.props.dispatch(
          setNotification({
            visible: true,
            message: "Termin uspješno rezerviran!",
            error: false
          })
        );
        this.setState(() => ({
          loading: false,
          reserved: true
        }));
      })
      .catch(err => {
        this.setState(() => {
          return {
            formError: true,
            formMessage: err.message,
            loading: false
          };
        });
      });
  };

  handleUserInput = e => {
    const inputName = e.target.name;
    const index = inputName.substring(5);
    const value = e.target.value;

    const prevState = { ...this.state };
    const users = prevState.users;

    users[index].value = value;

    this.setState(prevState => {
      return {
        users: users
      };
    });
    /**
     * Check if user is already on the list or if it's author
     */
    this.checkEmail(value)
      .then(id => {
        users[index].message = id ? "" : "Korisnik ne postoji!";
        users[index].id = id ? id : undefined;

        this.setState(prevState => {
          return {
            users: users
          };
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  checkEmail = email => {
    if (!email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
      return new Promise(resolve => {
        resolve(false);
      });
    }

    return fetch(`/api/users/checkemail`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email
      })
    })
      .then(res => {
        if (res.status !== 200) {
          const error = new Error();
          error.res = res.json();
          error.statusCode = 404;
          throw error;
        }
        return res.json();
      })
      .then(data => {
        return data.id;
      })
      .catch(err => {
        return false;
      });
  };

  handleDeleteUser = deleteIndex => {
    this.setState(prevState => {
      return {
        users: prevState.users.filter((user, index) => {
          if (index === deleteIndex) return false;
          return true;
        })
      };
    });
  };

  handleAddUser = () => {
    this.setState(prevState => {
      const newUser = {
        value: "",
        message: "Korisnikov email je obavezan!"
      };
      prevState.users.push(newUser);

      return {
        users: prevState.users
      };
    });
  };

  render() {
    if (!this.state.loaded) return <PageLoader />;
    if (this.state.reserved) return <Redirect to="/myreservations" />;

    return (
      <section id="reserve-page" className="page">
        <Helmet>
          <title>Rezervacija | Rezerviraj termin online</title>
        </Helmet>
        <div className="container">
          <h2 className="page-subtitle page-subtitle">
            {this.state.timeslot.sportfield.name} -{" "}
            {this.state.timeslot.company.name}
          </h2>
          <div className="reserve-timeslot">
            <h1 className="page-title page-title">Rezervacija</h1>
            <SectionTitle label="Odabrani termin" />
            <ul className="hl-timeslot">
              <TimeslotItem
                key={this.state.timeslot.id}
                id={this.state.timeslot.id}
                startTime={moment(this.state.timeslot.startTime).format(
                  "HH:mm"
                )}
                toLabel="Promijeni termin"
                to={`/field/${this.state.timeslot.sportfield.id}`}
                endTime={moment(this.state.timeslot.endTime).format("HH:mm")}
                price={`${this.state.timeslot.price}HRK`}
              />
            </ul>
          </div>

          <div className="reserve-users">
            <SectionTitle label="Ostali korisnici" />
            <p>Pridruži druge registrirane korisnike rezervaciji.</p>
            {this.state.users.map((user, index) => {
              return (
                <div className="input-container-delete" key={index}>
                  <InputContainer
                    message={user.message}
                    type="text"
                    name={`email${index}`}
                    className={
                      "inputFieldWhite " +
                      (!user.message
                        ? "inputFieldWhite--valid"
                        : "inputFieldWhite--error")
                    }
                    value={user.value}
                    placeholder="Email adresa korisnika"
                    onChange={this.handleUserInput}
                  />
                  <BtnDelete
                    onClick={e => {
                      this.handleDeleteUser(index);
                    }}
                  />
                </div>
              );
            })}
            <BtnAdd onClick={this.handleAddUser} />
          </div>

          <div className="reserve-end">
            <SectionTitle label="Dovrši rezervaciju" />
            <div className="conditions">
              <h4>Uvjeti</h4>
              <p>{this.state.timeslot.sportfield.conditions}</p>
            </div>
            <form onSubmit={this.onSubmit}>
              {!this.props.user.id && (
                <FormMessage
                  message="Potrebno se prijaviti u sustav kako bi dovršili rezervaciju."
                  error={false}
                />
              )}
              {this.props.user.id && (
                <React.Fragment>
                  <BtnGreen
                    label="Rezerviraj"
                    submit={true}
                    loading={this.state.loading}
                  />
                  {this.state.formMessage && (
                    <FormMessage
                      message={this.state.formMessage}
                      error={this.state.formError}
                    />
                  )}
                </React.Fragment>
              )}
            </form>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(ReservePage);
