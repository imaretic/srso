import React from "react";
import { Helmet } from "react-helmet";
import { Redirect } from "react-router";
import { connect } from "react-redux";
import CompanyForm from "../common/forms/CompanyForm";
import CompanyValidator from "../common/validators/CompanyValidator";

class MyCompanyCreatePage extends React.Component {
  handleSucess = () => {
    this.props.reloadUser();
  };

  render() {
    if (this.props.user.company !== null) return <Redirect to="/mycompany" />;

    return (
      <section id="register-page" className="page">
        <Helmet>
          <title>Registriraj tvrtku | Rezerviraj termin online</title>
        </Helmet>
        <div className="container">
          <h1 className="page-title">Registracija tvrtke</h1>
          <div className="page-form">
            <CompanyValidator>
              <CompanyForm onSuccess={this.props.reloadUser} />
            </CompanyValidator>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(MyCompanyCreatePage);
