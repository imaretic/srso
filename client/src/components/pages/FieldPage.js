import React from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import moment from "moment";
import "react-dates/initialize";
import { SingleDatePicker, isInclusivelyAfterDay } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import PageLoader from "../common/loader/PageLoader";
import SectionLoader from "../common/loader/SectionLoader";
import TimeslotItem from "../common/listItems/TimeslotItem";
import SectionTitle from "../common/util/SectionTitle";

class FieldPage extends React.Component {
  state = {
    loaded: false,
    reloaded: false,
    selectedDate: moment(),
    fieldData: {},
    companyData: {},
    data: []
  };

  onDateChange = selectedDate => {
    if (selectedDate) {
      this.setState(() => {
        return { selectedDate };
      }, this.reloadTimeslots);
    }
  };

  onFocusChange = ({ focused }) => {
    this.setState(() => ({ focused }));
  };

  componentDidMount = () => {
    fetch(`/api/fields/${this.props.match.params.id}`, {
      method: "GET"
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            loaded: true,
            fieldData: data.field,
            companyData: data.company
          };
        }, this.reloadTimeslots);
      })
      .catch(err => {
        console.log(err);
      });
  };

  reloadTimeslots = () => {
    this.setState(() => ({ reloaded: false }));

    fetch(`/api/timeslots/bydate`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        date: this.state.selectedDate.format(),
        sportfieldId: this.props.match.params.id
      })
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState(() => {
          return { loaded: true, reloaded: true, data: data };
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleDeleteSuccess = timeslotId => {
    this.setState(prevState => {
      return {
        data: prevState.data.filter(timeslot => {
          return timeslot.id !== timeslotId;
        })
      };
    });
  };

  handleOnDateChange = newDate => {
    this.setState(
      () => ({ selectedDate: newDate }),
      () => {
        this.reloadTimeslots();
      }
    );
  };

  render() {
    if (!this.state.loaded) return <PageLoader />;

    return (
      <section id="mycompany-page" className="page">
        <Helmet>
          <title>{this.state.fieldData.name} | Rezerviraj termin online</title>
        </Helmet>
        <div className="container">
          <h2 className="page-subtitle page-subtitle">
            {this.state.companyData.name}
          </h2>
          <h1 className="page-title page-title">{this.state.fieldData.name}</h1>
          <div className="field-datepicker">
            <h3>Odabrani datum</h3>
            <SingleDatePicker
              date={this.state.selectedDate}
              onDateChange={this.onDateChange}
              focused={this.state.focused}
              onFocusChange={this.onFocusChange}
              numberOfMonths={1}
              noBorder={true}
              isOutsideRange={day => !isInclusivelyAfterDay(day, moment())}
              hideKeyboardShortcutsPanel={true}
            />
          </div>
          <SectionTitle label="Dostupni termini" />
          {this.state.data.length === 0 && (
            <p className="section-title-message">Nema dostupnih termina.</p>
          )}
          {!this.state.reloaded && <SectionLoader />}
          <ul className="hl-timeslot">
            {this.state.data.map(timeslot => {
              return (
                <TimeslotItem
                  key={timeslot.id}
                  id={timeslot.id}
                  startTime={moment(timeslot.startTime).format("HH:mm")}
                  endTime={moment(timeslot.endTime).format("HH:mm")}
                  to={`/reserve/${timeslot.id}`}
                  toLabel="Rezerviraj"
                  reserved={!!timeslot.reservations.length}
                  price={`${timeslot.price}HRK`}
                />
              );
            })}
          </ul>
        </div>
      </section>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(FieldPage);
