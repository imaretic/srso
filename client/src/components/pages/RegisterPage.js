import React from "react";
import { Helmet } from "react-helmet";
import { Redirect } from "react-router";
import { connect } from "react-redux";
import RegisterForm from "../common/forms/RegisterForm";
import RegisterValidator from "../common/validators/RegisterValidator";

class RegisterPage extends React.Component {
  state = {
    createdAccount: false
  };

  handleSucess = () => {
    this.setState(() => {
      return { createdAccount: true };
    });
  };

  render() {
    if (this.state.createdAccount) {
      return <Redirect to="/login" />;
    }

    return (
      <section id="register-page" className="page">
        <Helmet>
          <title>Registracija | Rezerviraj termin online</title>
        </Helmet>
        <div className="container">
          <h1 className="page-title">Registracija</h1>
          <div className="page-form">
            <RegisterValidator>
              <RegisterForm />
            </RegisterValidator>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(RegisterPage);
