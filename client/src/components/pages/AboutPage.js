import React from "react";
import { Helmet } from "react-helmet";

class AboutPage extends React.Component {
  render() {
    return (
      <section className="page">
        <Helmet>
          <title>O Projektu | Rezerviraj termin online</title>
        </Helmet>
        <div className="container">
          <h1 className="page-title page-title--left">O Projektu</h1>
          <p className="page-content">
            Projekt nastao kao Završni rad. Više informacija uskoro.
            <br />
            Git repozitorij dostupan je na poveznici:{" "}
            <a
              href="https://gitlab.com/imaretic/srso"
              target="_blank"
              rel="noopener noreferrer"
            >
              https://gitlab.com/imaretic/srso
            </a>
          </p>
        </div>
      </section>
    );
  }
}

export default AboutPage;
