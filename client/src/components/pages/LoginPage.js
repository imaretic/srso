import React from "react";
import { Helmet } from "react-helmet";
import LoginForm from "../common/forms/LoginForm";
import RegisterValidator from "../common/validators/RegisterValidator";

class LoginPage extends React.Component {
  render() {
    return (
      <section id="login-page" className="page">
        <Helmet>
          <title>Prijava | Rezerviraj termin online</title>
        </Helmet>
        <div className="container">
          <h1 className="page-title">Prijava</h1>
          <div className="page-form page-form--center">
            <RegisterValidator>
              <LoginForm />
            </RegisterValidator>
          </div>
        </div>
      </section>
    );
  }
}

export default LoginPage;
