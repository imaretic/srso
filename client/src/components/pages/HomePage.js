import React from "react";
import { Helmet } from "react-helmet";
import SectionTitle from "../common/util/SectionTitle";
import field3D from "../../img/field3D.png";
import CompanyItem from "../common/listItems/CompanyItem";
import PageLoader from "../common/loader/PageLoader";

class HomePage extends React.Component {
  state = {
    loaded: false,
    data: []
  };

  componentDidMount = () => {
    fetch(`/api/companies`, {
      method: "GET"
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState(() => {
          return { loaded: true, data: data };
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    if (!this.state.loaded) return <PageLoader />;

    return (
      <React.Fragment>
        <Helmet>
          <title>Naslovnica | Rezerviraj termin online</title>
        </Helmet>

        <header id="home-header">
          <div className="container">
            <h1>
              <strong>Pronađi najbolji termin</strong>
              <br />
              za svoju ekipu
            </h1>
            <img src={field3D} alt="3D field" />
          </div>
        </header>

        <section id="available-objects">
          <div className="container">
            <SectionTitle label="Dostupni objekti" />
            <ul className="hl-company">
              {this.state.data.map(company => {
                return (
                  <CompanyItem
                    key={company.id}
                    id={company.id}
                    name={company.name}
                    address={`${company.street} ${company.streetNumber}`}
                  />
                );
              })}
            </ul>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

export default HomePage;
