import React from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import Tabs from "../common/tabs/Tabs";
import RegisterValidator from "../common/validators/RegisterValidator";
import EditProfileForm from "../common/forms/EditProfileForm";
import PageLoader from "../common/loader/PageLoader";

class ProfilePage extends React.Component {
  state = {
    loaded: true
  };

  handleSucess = () => {
    this.props.reloadUser();
  };

  render() {
    if (this.state.loaded === false) return <PageLoader />;

    return (
      <section id="mycompany-page" className="page">
        <Helmet>
          <title>Moj račun | Rezerviraj termin online</title>
        </Helmet>
        <div className="container">
          <h2 className="page-subtitle page-subtitle--left">Moj račun</h2>
          <h1 className="page-title page-title--left">
            {this.props.user.firstName + " " + this.props.user.lastName}
          </h1>

          <Tabs
            content={[
              {
                label: "Postavke profila",
                content: index => {
                  return (
                    <React.Fragment key={index}>
                      <div className="tabs__content__left">
                        <div className="page-form page-form--left">
                          <RegisterValidator>
                            <EditProfileForm
                              onSuccess={this.props.reloadUser}
                            />
                          </RegisterValidator>
                        </div>
                      </div>
                      <div className="tabs__content__right">
                        <div className="tabs__content__bottom" />
                      </div>
                    </React.Fragment>
                  );
                }
              }
            ]}
          />
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(ProfilePage);
