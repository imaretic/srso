import React from "react";
import { Helmet } from "react-helmet";
import moment from "moment";
import { connect } from "react-redux";
import PageLoader from "../common/loader/PageLoader";
import PollItem from "../common/listItems/PollItem";

class MyPollsPage extends React.Component {
  state = {
    loaded: false,
    data: []
  };

  componentDidMount = () => {
    fetch(`/api/polls/byuser/${this.props.user.id}`, {
      method: "GET"
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            loaded: true,
            data: data
          };
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    if (!this.state.loaded) return <PageLoader />;

    return (
      <section id="mypolls-page" className="page">
        <Helmet>
          <title>Moje ankete | Rezerviraj termin online</title>
        </Helmet>
        <div className="container">
          <h1 className="page-title page-title--left">Moje ankete</h1>
          {this.state.data.length === 0 && (
            <React.Fragment>
              <p className="page-message">Nema stvorenih anketa.</p>
            </React.Fragment>
          )}
          <ul className="hl-poll">
            {this.state.data.map(poll => {
              return (
                <PollItem
                  id={poll.id}
                  name={`Anketa (${moment(poll.createdAt).format(
                    "D.M.YYYY u HH:m"
                  )})`}
                />
              );
            })}
          </ul>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(MyPollsPage);
