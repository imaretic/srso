import React from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import EditCompanyForm from "../common/forms/EditCompanyForm";
import FieldForm from "../common/forms/FieldForm";
import Tabs from "../common/tabs/Tabs";
import CompanyValidator from "../common/validators/CompanyValidator";
import FieldValidator from "../common/validators/FieldValidator";
import FieldItem from "../common/listItems/FieldItem";
import SectionLoader from "../common/loader/SectionLoader";

class CompanyPage extends React.Component {
  state = {
    loaded: false,
    data: []
  };

  componentDidMount = () => {
    if (this.props.user.company === null) return;

    const companyId = this.props.user.company.id;
    fetch(`/api/fields/bycompany/${companyId}`, {
      method: "GET"
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState(() => {
          return { loaded: true, data: data };
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  onDeleteSuccess = fieldId => {
    this.setState(prevState => {
      return {
        data: prevState.data.filter(field => {
          return field.id !== fieldId;
        })
      };
    });
  };

  onCreateSuccess = newSportfield => {
    this.setState(prevState => {
      return { data: [...prevState.data, newSportfield.sportfield] };
    });
  };

  handleDelete = () => {
    const token = localStorage.getItem("token");
    fetch(`/companies/${this.props.user.company.id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => {
        if (res.status !== 200) {
          const error = new Error("Pogreška pri brisanju tvrtke.");
          error.statusCode = res.status;
          throw error;
        }
        return res.json();
      })
      .then(resData => {
        this.setState(() => {
          this.props.reloadUser();
        });
      })
      .catch(err => {
        this.setState(() => ({
          loaded: true
        }));
      });
  };

  render() {
    if (this.props.user.company === null)
      return <Redirect to="/mycompany/create" />;

    return (
      <section id="mycompany-page" className="page">
        <Helmet>
          <title>Moja tvrtka | Rezerviraj termin online</title>
        </Helmet>
        <div className="container">
          <h2 className="page-subtitle page-subtitle--left">Moja tvrtka</h2>
          <h1 className="page-title page-title--left">
            {this.props.user.company.name}
          </h1>

          <Tabs
            content={[
              {
                label: "Tereni",
                content: index => {
                  return (
                    <div className="tabs__content__full" key={index}>
                      {!this.state.loaded && <SectionLoader />}
                      {this.state.loaded && (
                        <ul className="h-list--small">
                          {this.state.data.map(field => {
                            return (
                              <FieldItem
                                id={field.id}
                                key={field.id}
                                name={field.name}
                                note={field.sport.name}
                                onDeleteSuccess={this.onDeleteSuccess}
                              />
                            );
                          })}
                        </ul>
                      )}
                      <div className="page-form">
                        <FieldValidator>
                          <FieldForm onSuccess={this.onCreateSuccess} />
                        </FieldValidator>
                      </div>
                    </div>
                  );
                }
              },
              {
                label: "Podaci",
                content: index => {
                  return (
                    <React.Fragment key={index}>
                      <div className="tabs__content__left">
                        <div className="page-form page-form--left">
                          <CompanyValidator>
                            <EditCompanyForm
                              onSuccess={this.props.reloadUser}
                            />
                          </CompanyValidator>
                        </div>
                      </div>
                      <div className="tabs__content__right">
                        <div className="tabs__content__bottom" />
                      </div>
                    </React.Fragment>
                  );
                }
              }
            ]}
          />
        </div>
      </section>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(CompanyPage);
