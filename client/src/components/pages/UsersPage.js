import React from "react";
import { Helmet } from "react-helmet";
import PageLoader from "../common/loader/PageLoader";
import AdminUserItem from "../common/listItems/AdminUserItem";

class UsersPage extends React.Component {
  state = {
    users: []
  };

  componentDidMount() {
    const token = localStorage.getItem("token");
    fetch(`/api/users`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => {
        // Validation failed
        if (res.status === 422) {
          const error = new Error();
          error.res = res.json();
          error.statusCode = 422;
          throw error;
        }

        // Some other problem occured
        if (res.status !== 200 && res.status !== 201) {
          const error = new Error(
            "Nesporazum u komunikaciji s bazom. Molimo pokušajte ponovo."
          );
          error.statusCode = res.status;
          throw error;
        }

        return res.json();
      })
      .then(resData => {
        this.setState(prevState => {
          return { users: resData, loaded: true };
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  handleToggleAdminSuccess = userId => {
    console.log("Administrator promijenjen!");
    this.setState(prevState => {
      const { isAdmin: oldRole } = prevState.users.find(user => {
        return user.id === userId;
      });
      console.log("Stara vrijednost: ", oldRole);
      return {
        users: prevState.users.map(user => {
          if (user.id === userId) {
            user.isAdmin = !oldRole;
          }
          return user;
        })
      };
    });
  };

  handleDeleteUserSuccess = userId => {
    this.setState(prevState => {
      return {
        users: prevState.users.filter(user => {
          return user.id !== userId;
        })
      };
    });
  };

  render() {
    if (!this.state.loaded) return <PageLoader />;

    return (
      <section id="users-page" className="page">
        <Helmet>
          <title>Korisnici | Rezerviraj termin online</title>
        </Helmet>
        <div className="container">
          <h1 className="page-title page-title--admin">Korisnici</h1>
          <ul className="users-page__list">
            {this.state.users.map(user => {
              return (
                <AdminUserItem
                  key={user.id}
                  id={user.id}
                  firstName={user.firstName}
                  lastName={user.lastName}
                  email={user.email}
                  phone={user.phone}
                  country={user.country}
                  isAdmin={user.isAdmin}
                  onToggleAdminSuccess={this.handleToggleAdminSuccess}
                  onDeleteUserSuccess={this.handleDeleteUserSuccess}
                />
              );
            })}
          </ul>
        </div>
      </section>
    );
  }
}

export default UsersPage;
