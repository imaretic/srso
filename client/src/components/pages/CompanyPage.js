import React from "react";
import { Helmet } from "react-helmet";
import SectionTitle from "../common/util/SectionTitle";
import phone from "../../img/contact/phone.svg";
import email from "../../img/contact/email.svg";
import location from "../../img/contact/location.svg";
import FieldCardItem from "../common/listItems/FieldCardItem";
import PageLoader from "../common/loader/PageLoader";

class CompanyPage extends React.Component {
  state = {
    loaded: false,
    loadedFields: false,
    data: undefined,
    dataFields: []
  };

  componentDidMount = () => {
    fetch(`/api/companies/${this.props.match.params.id}`, {
      method: "GET"
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState(() => {
          return { loaded: true, data: data };
        });
      })
      .catch(err => {
        console.log(err);
      });

    fetch(`/api/fields/bycompany/${this.props.match.params.id}`, {
      method: "GET"
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState(() => {
          return { loadedFields: true, dataFields: data };
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    if (!this.state.loaded || !this.state.loadedFields) return <PageLoader />;

    return (
      <React.Fragment>
        <Helmet>
          <title>{this.state.data.name} | Rezerviraj termin online</title>
        </Helmet>
        <header id="company-page-header">
          <div className="container">
            <h2 className="page-subtitle">Tvrtka</h2>
            <h1>{this.state.data.name}</h1>
          </div>
        </header>
        <section id="available-fields">
          <div className="container">
            <SectionTitle label="Dostupni tereni" />
            <ul className="field-card-list">
              {this.state.dataFields.map(field => {
                return (
                  <FieldCardItem
                    id={field.id}
                    key={field.id}
                    name={field.name}
                    numberOfPlayers={field.numberOfPlayers}
                    sport={field.sport.name}
                    imageUrl={field.sport.imageUrl}
                  />
                );
              })}
            </ul>
          </div>
        </section>
        <section id="company-page-contact">
          <div className="container">
            <div className="company-page-contact__title">
              <h2>Kontaktiraj</h2>
              <p>Direktno stupi u kontakt</p>
            </div>
            <div className="company-page-contact__info">
              <div className="contact-row">
                <div className="contact-row__icon">
                  <img src={phone} alt="Telefonski broj" />
                </div>
                <div className="contact-row__content">
                  {this.state.data.phone}
                </div>
              </div>
              <div className="contact-row">
                <div className="contact-row__icon">
                  <img src={email} alt="Email" />
                </div>
                <div className="contact-row__content">
                  {this.state.data.email}
                </div>
              </div>
              <div className="contact-row">
                <div className="contact-row__icon">
                  <img src={location} alt="Lokacija" />
                </div>
                <div className="contact-row__content">
                  {this.state.data.street} {this.state.data.streetNumber},
                  <br />
                  {this.state.data.postCode} {this.state.data.city}, <br />
                  {this.state.data.country}
                </div>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

export default CompanyPage;
