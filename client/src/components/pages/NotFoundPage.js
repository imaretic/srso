import React from "react";
import { Helmet } from "react-helmet";

class NotFoundPage extends React.Component {
  render() {
    return (
      <div className="container">
        <Helmet>
          <title>Stranica nije pronađena | Rezerviraj termin online</title>
        </Helmet>
        <div className="page-notfound">
          <span className="code">404</span>
          <span className="status">Page Not Found</span>
        </div>
      </div>
    );
  }
}

export default NotFoundPage;
