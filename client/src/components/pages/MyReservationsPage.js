import React from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { setNotification } from "../../actions/notification";
import PageLoader from "../common/loader/PageLoader";
import TimeslotItemExp from "../common/listItems/TimeslotItemExp";
import moment from "moment";

class MyReservationsPage extends React.Component {
  state = {
    loaded: false,
    data: []
  };

  componentDidMount = () => {
    this.reloadParticipations();
  };

  reloadParticipations = () => {
    fetch(`/api/reservations/byuser/${this.props.user.id}`, {
      method: "GET"
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            loaded: true,
            data: data
          };
        });
      })
      .catch(err => {
        this.props.dispatch(
          setNotification({
            message: "Došlo je do pogreške",
            error: true
          })
        );
        console.log(err);
      });
  };

  handlePaymentSuccess = participationId => {
    this.reloadParticipations();
  };

  render() {
    if (!this.state.loaded) return <PageLoader />;

    return (
      <section id="reserve-page" className="page">
        <Helmet>
          <title>Moje rezervacije | Rezerviraj termin online</title>
        </Helmet>
        <div className="container">
          <h1 className="page-title page-title--left">Moje rezervacije</h1>
          {this.state.data.length === 0 && (
            <p className="page-message">Nema rezervacija.</p>
          )}
          <ul className="hl-timeslot">
            {this.state.data.map(participation => {
              return (
                <TimeslotItemExp
                  key={participation.id}
                  id={participation.id}
                  startTime={moment(
                    participation.reservation.timeslot.startTime
                  ).format("HH:mm")}
                  endTime={moment(
                    participation.reservation.timeslot.endTime
                  ).format("HH:mm")}
                  date={moment(
                    participation.reservation.timeslot.startTime
                  ).format("L")}
                  author={`${participation.reservation.user.firstName} ${
                    participation.reservation.user.lastName
                  }`}
                  price={`${participation.reservation.timeslot.price}HRK`}
                  field={participation.reservation.timeslot.sportfield.name}
                  fieldId={participation.reservation.timeslot.sportfield.id}
                  company={
                    participation.reservation.timeslot.sportfield.company.name
                  }
                  companyId={
                    participation.reservation.timeslot.sportfield.company.id
                  }
                  participationId={participation.id}
                  reservationId={participation.reservation.id}
                  paid={participation.paid}
                  onPaymentSuccess={this.handlePaymentSuccess}
                  users={participation.reservation.participations.map(
                    participation => {
                      return participation.user;
                    }
                  )}
                />
              );
            })}
          </ul>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(MyReservationsPage);
