import React from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import moment from "moment";
import TimeslotValidator from "../common/validators/TimeslotValidator";
import SectionLoader from "../common/loader/SectionLoader";
import TimeslotForm from "../common/forms/TimeslotForm";
import TimeslotItem from "../common/listItems/TimeslotItem";
import SectionTitle from "../common/util/SectionTitle";
import PageLoader from "../common/loader/PageLoader";

class MyCompanyFieldPage extends React.Component {
  state = {
    timeslotsLoaded: false,
    fieldLoaded: false,
    fieldData: {},
    reloaded: false,
    selectedDate: moment(),
    timeslotsData: undefined
  };

  componentDidMount = () => {
    fetch(`/api/fields/${this.props.match.params.id}`, {
      method: "GET"
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            fieldLoaded: true,
            fieldData: data.field
          };
        });
      })
      .catch(err => {
        console.log(err);
      });
    this.reloadTimeslots();
  };

  reloadTimeslots = () => {
    this.setState(() => ({ reloaded: false }));
    fetch(`/api/timeslots/bydate`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        date: this.state.selectedDate.format(),
        sportfieldId: this.props.match.params.id
      })
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            timeslotsLoaded: true,
            reloaded: true,
            timeslotsData: data
          };
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleDeleteSuccess = timeslotId => {
    this.setState(prevState => {
      return {
        timeslotsData: prevState.timeslotsData.filter(timeslot => {
          return timeslot.id !== timeslotId;
        })
      };
    });
  };

  handleOnDateChange = newDate => {
    this.setState(
      () => ({ selectedDate: newDate }),
      () => {
        this.reloadTimeslots();
      }
    );
  };

  render() {
    if (!this.state.timeslotsLoaded && !this.statefieldLoaded)
      return <PageLoader />;

    return (
      <section id="mycompany-page" className="page">
        <Helmet>
          <title>Moj tereni | Rezerviraj termin online</title>
        </Helmet>
        <div className="container">
          <h2 className="page-subtitle page-subtitle--left">
            {this.props.user.company.name}
          </h2>
          <h1 className="page-title page-title--left">
            {this.state.fieldData.name}
          </h1>
          <TimeslotValidator>
            <TimeslotForm
              onDateChange={this.handleOnDateChange}
              id={this.props.match.params.id}
              onSuccess={this.reloadTimeslots}
            />
          </TimeslotValidator>
          <SectionTitle label="Postojeći termini na ovaj datum" />
          {this.state.timeslotsData.length === 0 && (
            <p className="section-title-message">Nema dostupnih termina.</p>
          )}
          {!this.state.reloaded && <SectionLoader />}
          <ul className="hl-timeslot">
            {this.state.timeslotsData.map(timeslot => {
              return (
                <TimeslotItem
                  key={timeslot.id}
                  id={timeslot.id}
                  startTime={moment(timeslot.startTime).format("HH:mm")}
                  endTime={moment(timeslot.endTime).format("HH:mm")}
                  onDeleteSuccess={this.handleDeleteSuccess}
                  reserved={!!timeslot.reservations.length}
                  price={`${timeslot.price}HRK`}
                />
              );
            })}
          </ul>
        </div>
      </section>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(MyCompanyFieldPage);
