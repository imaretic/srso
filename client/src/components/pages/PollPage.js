import React from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import PageLoader from "../common/loader/PageLoader";
import PollOptionItem from "../common/listItems/PollOptionItem";
import moment from "moment";

class PollPage extends React.Component {
  state = {
    data: [],
    loaded: false,
    totalVotes: 1,
    votable: true
  };

  componentDidMount = () => {
    this.reloadPollOptions();
  };

  reloadPollOptions = () => {
    fetch(`/api/polls/${this.props.match.params.id}`, {
      method: "GET"
    })
      .then(res => {
        return res.json();
      })
      .then(data => {
        let totalVotes = 0;
        data.polloptions.forEach(polloption => {
          polloption.votes.forEach(vote => {
            if (vote.userId === this.props.user.id) {
              this.setState(() => {
                return { votable: false };
              });
            }
            ++totalVotes;
          });
        });

        this.setState(() => {
          return {
            loaded: true,
            data: data,
            totalVotes
          };
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleVoteSuccess = () => {
    this.reloadPollOptions();
    this.setState(() => {
      return { votable: false };
    });
  };

  render() {
    if (!this.state.loaded) return <PageLoader />;

    return (
      <section id="poll-page" className="page">
        <Helmet>
          <title>Anketa | Rezerviraj termin online</title>
        </Helmet>
        <div className="container">
          <h1 className="page-title page-title--left">Anketa</h1>

          <p className="page-content">
            Sudjelovanje u anketi dozvoljeno je samo jednom.
          </p>
          <ul className="hl-polloption">
            {this.state.data.polloptions.map(polloption => {
              const totalVotes = parseFloat(this.state.totalVotes);
              let percentage = 0;
              if (totalVotes !== 0) {
                percentage = (
                  (parseFloat(polloption.votes.length) / totalVotes) *
                  100
                ).toFixed(0);
              }

              return (
                <PollOptionItem
                  key={polloption.id}
                  id={polloption.id}
                  votable={this.state.votable}
                  startTime={moment(polloption.timeslot.startTime).format(
                    "HH:mm"
                  )}
                  endTime={moment(polloption.timeslot.endTime).format("HH:mm")}
                  date={moment(polloption.timeslot.startTime).format("L")}
                  price={`${polloption.timeslot.price}HRK`}
                  sportfield={polloption.timeslot.sportfield.name}
                  company={polloption.timeslot.sportfield.company.name}
                  onVoteSuccess={this.handleVoteSuccess}
                  percentage={percentage}
                />
              );
            })}
          </ul>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

export default connect(mapStateToProps)(PollPage);
