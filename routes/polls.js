const express = require("express");
const pollsController = require("../controllers/poll");
const { body } = require("express-validator/check");
const isAuth = require("../middleware/is-auth");

const router = express.Router();

router.put("/create", isAuth, pollsController.putCreate);
router.get("/byuser/:id", pollsController.getByUser);
router.get("/:id", pollsController.getPoll);

module.exports = router;
