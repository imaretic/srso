const express = require("express");
const { body } = require("express-validator/check");
const authController = require("../controllers/auth");
const isAuth = require("../middleware/is-auth");

const User = require("../models/user");

const router = express.Router();
const MIN_PW_LENGTH = 6;

const validator = edit => {
  return [
    body("firstName")
      .not()
      .isEmpty()
      .withMessage("Please enter a valid first name"),
    body("lastName")
      .not()
      .isEmail()
      .withMessage("Please enter a valid last name"),
    body("phone")
      .isNumeric()
      .withMessage("Please enter a valid phone number"),
    body("country")
      .not()
      .isEmpty()
      .withMessage("Please enter a valid country"),
    body("email")
      .isEmail()
      .withMessage("Please enter a valid email")
      .custom((value, { req }) => {
        return User.findOne({ where: { email: value } }).then(user => {
          if (user && edit === false) {
            throw new Error("E-Mail address already exists!");
          }
        });
      }),
    body("password")
      .isLength({ min: MIN_PW_LENGTH })
      .withMessage(
        `Password must be at least ${MIN_PW_LENGTH} characters long.`
      )
  ];
};

router.post(
  "/login",
  [
    body("email")
      .isEmail()
      .withMessage("Please enter a valid email.")
  ],
  authController.login
);

router.put("/signup", validator(false), authController.signup);
router.post("/edit", isAuth, validator(true), authController.edit);

module.exports = router;
