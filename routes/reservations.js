const express = require("express");
const reservationController = require("../controllers/reservation");
const isAuth = require("../middleware/is-auth");

const router = express.Router();

router.get("/", reservationController.getReservations);
router.put("/create", isAuth, reservationController.putCreate);
router.get("/byuser/:id", reservationController.getReservationsByUser);

module.exports = router;
