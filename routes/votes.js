const express = require("express");
const votesController = require("../controllers/vote");
const { body } = require("express-validator/check");
const isAuth = require("../middleware/is-auth");

const router = express.Router();

router.put("/vote/:id", isAuth, votesController.putVote);

module.exports = router;
