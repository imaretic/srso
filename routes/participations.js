const express = require("express");
const participationController = require("../controllers/participation");
const isAuth = require("../middleware/is-auth");

const router = express.Router();

router.post("/pay/:id", participationController.postPay);

module.exports = router;
