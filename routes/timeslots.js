const express = require("express");
const timeslotsController = require("../controllers/timeslots");
const { body } = require("express-validator/check");
const isAuth = require("../middleware/is-auth");

const router = express.Router();

router.get("/", timeslotsController.getTimeslots);
router.get("/:id", timeslotsController.getTimeslot);
router.post("/bylist", timeslotsController.getTimeslotsByList);
router.post(
  "/bydate",
  [
    body("sportfieldId")
      .isInt()
      .not()
      .isEmpty()
      .withMessage("Sportfield ID is required!"),
    body("list")
      .isEmpty()
      .withMessage("List of Timeslot IDs is required!")
  ],
  timeslotsController.postByDate
);
router.delete("/:id", isAuth, timeslotsController.deleteTimeslot);
router.put(
  "/create",
  isAuth,
  [
    body("startTime")
      .trim()
      .not()
      .isEmpty()
      .withMessage("Start time is required!"),
    body("numberOfTimeslots")
      .trim()
      .not()
      .isEmpty()
      .withMessage("Number of timeslots is required!"),
    body("timeslotDuration")
      .trim()
      .not()
      .isEmpty()
      .withMessage("Timeslot duration is required!"),
    body("sportfieldId")
      .trim()
      .not()
      .isEmpty()
      .withMessage("Sportfield ID is required!"),
    body("price")
      .trim()
      .not()
      .isEmpty()
      .withMessage("Price is required!")
  ],
  isAuth,
  timeslotsController.putCreate
);

module.exports = router;
