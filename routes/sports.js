const express = require("express");
const sportsController = require("../controllers/sports");
const isAuth = require("../middleware/is-auth");

const router = express.Router();

router.delete("/delete/:id", isAuth, sportsController.deleteSport);
router.put("/create", sportsController.putCreate);
router.get("/", sportsController.getSports);

module.exports = router;
