const express = require("express");
const fieldController = require("../controllers/field");
const { body } = require("express-validator/check");

const router = express.Router();

router.put(
  "/create",
  [
    body("name")
      .trim()
      .isLength({ min: 3 })
      .withMessage("Field name must have at least 3 characters."),
    body("numberOfPlayers")
      .trim()
      .isLength({ min: 1 })
      .withMessage("Number of players are required."),
    body("conditions")
      .trim()
      .isLength({ min: 3 })
      .withMessage("Conditions name must have at least 3 characters.")
  ],
  fieldController.putCreate
);
router.get("/bycompany/:id", fieldController.getFieldsByCompany);
router.delete("/:id", fieldController.deleteField);
router.get("/:id", fieldController.getField);
router.get("/", fieldController.getFields);

module.exports = router;
