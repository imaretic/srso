const express = require("express");
const { body } = require("express-validator/check");
const userController = require("../controllers/user");
const isAuth = require("../middleware/is-auth");
const User = require("../models/user");

const router = express.Router();

router.delete("/:id", userController.deleteUser);
router.post(
  "/toggleadmin",
  isAuth,
  [
    body("userId")
      .isInt()
      .withMessage("Please enter a valid user id.")
      .custom((value, { req }) => {
        return User.findByPk(req.userId).then(user => {
          if (user.isAdmin === false) {
            throw new Error("User does not have administrator permissions!");
          }
        });
      })
  ],
  userController.postToggleAdmin
);
router.get("/:id", isAuth, userController.getUser);
router.post("/checkemail", userController.postCheckEmail);
router.get("/", isAuth, userController.getUsers);

module.exports = router;
