const express = require("express");
const companyController = require("../controllers/company");
const isAuth = require("../middleware/is-auth");
const { body } = require("express-validator/check");

const router = express.Router();

const validator = [
  body("name")
    .trim()
    .isLength({ min: 3 })
    .withMessage("Company name must have at least 3 characters."),
  body("email")
    .isEmail()
    .withMessage("E-mail is not valid"),
  body("phone")
    .trim()
    .not()
    .isEmpty()
    .withMessage("Phone number is not valid"),
  body("street")
    .trim()
    .not()
    .isEmpty()
    .withMessage("Street is not valid"),
  body("streetNumber")
    .trim()
    .not()
    .isEmpty()
    .withMessage("Street number is not valid"),
  body("city")
    .trim()
    .not()
    .isEmpty()
    .withMessage("City is not valid"),
  body("postCode")
    .trim()
    .not()
    .isEmpty()
    .withMessage("Post code is not valid"),
  body("country")
    .trim()
    .not()
    .isEmpty()
    .withMessage("Country is not valid")
];

router.get("/byuser/:id", companyController.getCompanyByUser);
router.put("/create", isAuth, validator, companyController.createCompany);
router.post("/:id", isAuth, validator, companyController.editCompany);
router.delete("/:id", companyController.deleteCompany);
router.get("/:id", companyController.getCompany);
router.get("/", companyController.getCompanies);

module.exports = router;
