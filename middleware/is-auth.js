const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  const authHeader = req.get("Authorization");
  if (!authHeader) {
    const error = new Error("Not authenticated. (1)");
    error.statusCode = 401;
    throw error;
  }
  const splittedToken = authHeader.split(" ");
  if (splittedToken.length != 2) {
    const error = new Error("Not authenticated. (2)");
    error.statusCode = 401;
    throw error;
  }
  const token = splittedToken[1];

  let decodedToken;
  try {
    decodedToken = jwt.verify(token, "secretkey");
  } catch (err) {
    err.statusCode = 500;
    throw err;
  }

  if (!decodedToken) {
    const error = new Error("Not authenticated. (3)");
    error.statusCode = 401;
    throw error;
  }
  console.log("========= AUTHORIZED ===========");
  console.log("USER ID IS: " + decodedToken.userId);
  req.userId = decodedToken.userId;
  next();
};
