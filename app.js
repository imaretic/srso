const express = require("express");
const bodyParser = require("body-parser");
const sequelize = require("./util/database");
const Sequelize = require("sequelize");
const fs = require("fs");
const path = require("path");
const helmet = require("helmet");
const morgan = require("morgan");
const multer = require("multer");
const uuidv4 = require("uuid/v4");

const startTime = require("./util/date");

// Routes
const authRoutes = require("./routes/auth");
const userRoutes = require("./routes/users");
const companyRoutes = require("./routes/companies");
const fieldRoutes = require("./routes/fields");
const reservationRoutes = require("./routes/reservations");
const timeslotRoutes = require("./routes/timeslots");
const sportRoutes = require("./routes/sports");
const participationRoutes = require("./routes/participations");
const pollRoutes = require("./routes/polls");
const voteRoutes = require("./routes/votes");

// Models
const User = require("./models/user");
const Company = require("./models/company");
const Sportfield = require("./models/sportfield");
const Timeslot = require("./models/timeslot");
const Reservation = require("./models/reservation");
const Participation = require("./models/participation");
const Sport = require("./models/sport");
const Poll = require("./models/poll");
const Polloption = require("./models/polloption");
const Vote = require("./models/vote");

const app = express();
app.use(bodyParser.json());

/**
 * Multer configuration
 */
const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "images");
  },
  filename: (req, file, cb) => {
    cb(null, uuidv4() + "-" + file.originalname);
  }
});
const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
app.use(
  multer({ storage: fileStorage, fileFilter: fileFilter }).single("image")
);

// Security Headers
app.use(helmet());

// Logs
const accessLogStream = fs.createWriteStream(
  path.join(__dirname, "access.log"),
  { flags: "a" }
);
app.use(morgan("combined", { stream: accessLogStream }));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "OPTIONS, GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

app.use("/api/auth", authRoutes);
app.use("/api/users", userRoutes);
app.use("/api/companies", companyRoutes);
app.use("/api/fields", fieldRoutes);
app.use("/api/reservations", reservationRoutes);
app.use("/api/timeslots", timeslotRoutes);
app.use("/api/sports", sportRoutes);
app.use("/api/participations", participationRoutes);
app.use("/api/polls", pollRoutes);
app.use("/api/votes", voteRoutes);

/**
 * Serve REACT frontend.
 */
app.use(express.static(path.join(__dirname, "client/build")));
app.use("/images", express.static(path.join(__dirname, "images")));
app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "client/build/index.html"));
});

/**
 * hasOne() -> adds link attribute to TARGET relation
 * belongsTo() -> adds link attribute to SOURCE relation
 */

User.hasOne(Company);
Sportfield.belongsTo(Company);
Sportfield.belongsTo(Sport, {
  foreignKey: { name: "sportId", allowNull: false }
});

Timeslot.belongsTo(Sportfield, {
  foreignKey: { name: "sportfieldId", allowNull: false }
});

Reservation.belongsTo(User, {
  foreignKey: { name: "userId", allowNull: false }
});
Reservation.belongsTo(Timeslot, {
  foreignKey: { name: "timeslotId", allowNull: false }
});
Timeslot.hasMany(Reservation);

Participation.belongsTo(User, {
  foreignKey: { name: "userId", allowNull: false }
});
Participation.belongsTo(Reservation, {
  foreignKey: { name: "reservationId", allowNull: false }
});
Reservation.hasMany(Participation);

Poll.belongsTo(User, {
  foreignKey: { name: "userId", allowNull: false }
});
User.hasMany(Poll);

Vote.belongsTo(User, {
  foreignKey: { name: "userId", allowNull: false }
});
Vote.belongsTo(Polloption, {
  foreignKey: { name: "polloptionId", allowNull: false }
});
Polloption.hasMany(Vote);

Polloption.belongsTo(Poll, {
  foreignKey: { name: "pollId", allowNull: false }
});
Polloption.belongsTo(Timeslot, {
  foreignKey: { name: "timeslotId", allowNull: false }
});
Poll.hasMany(Polloption);
Timeslot.hasMany(Polloption);

app.use((error, req, res, next) => {
  console.log(error);
  const status = error.statusCode || 500;
  const message = error.message;
  res.status(status).json({ message: message, data: error.data });
});

sequelize.sync();
app.listen(process.env.PORT || 5000);
