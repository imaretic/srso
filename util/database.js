const Sequelize = require("sequelize");

const HOST = process.env.PG_HOST;
const DATABASE = process.env.PG_DATABASE;
const USERNAME = process.env.PG_USERNAME;
const PASSWORD = process.env.PG_PASSWORD;

const sequelize = new Sequelize(DATABASE, USERNAME, PASSWORD, {
  host: HOST,
  dialect: "postgres",
  protocol: "postgres",
  dialectOptions: {
    ssl: true
  }
});

module.exports = sequelize;
