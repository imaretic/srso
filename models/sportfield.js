const Sequelize = require("sequelize");
const sequelize = require("../util/database");

const Sportfield = sequelize.define("sportfield", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  numberOfPlayers: {
    type: Sequelize.INTEGER,
    allowNull: true
  },
  conditions: {
    type: Sequelize.STRING,
    allowNull: false
  }
});

module.exports = Sportfield;
