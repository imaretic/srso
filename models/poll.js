const Sequelize = require("sequelize");
const sequelize = require("../util/database");

const Poll = sequelize.define("poll", {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true
  }
});

module.exports = Poll;
