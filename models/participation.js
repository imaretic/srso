const Sequelize = require("sequelize");
const sequelize = require("../util/database");

const Participation = sequelize.define("participation", {
  paid: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
    allowNull: false
  }
});

module.exports = Participation;
