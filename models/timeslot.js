const Sequelize = require("sequelize");
const sequelize = require("../util/database");

const Timeslot = sequelize.define("timeslot", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  startTime: {
    type: Sequelize.STRING,
    defaultValue: Sequelize.NOW
  },
  endTime: {
    type: Sequelize.STRING,
    defaultValue: Sequelize.NOW
  },
  price: {
    type: Sequelize.DOUBLE,
    allowNull: false
  }
});

module.exports = Timeslot;
