const Sequelize = require("sequelize");
const sequelize = require("../util/database");

const Polloption = sequelize.define("polloption", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  }
});

module.exports = Polloption;
